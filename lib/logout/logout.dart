import 'package:flutter/material.dart';

class Logout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Logged Out'),
      ),
      body: Column(
        children: <Widget>[
          Text('You have successfully logged out.'),
          RaisedButton(
            child: Text('Log back in'),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(context, '/login', (Route<dynamic> route) => false);
            },
          ),
        ]
      ),
    );
  }

}