import 'package:flutter/material.dart';
import 'package:mob_app/provider/db.dart';
import 'home/home.dart';

//void main() {
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await DB.init();
  
  runApp(HomeApp());
}