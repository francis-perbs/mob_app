import 'package:mob_app/models/model.dart';

class OvertimeDetailObj extends Model {
  static String table = 'overtime_detail';

  int id;
  int otID;
  String otNbr;
  String lineStatus;
  String date;
  String serviceType;
  String otDescr;
  String joNbr;
  String otdstart;
  String otdend;
  String lastModifiedDateTime;

  OvertimeDetailObj({this.id, this.otID, this.otNbr, this.lineStatus, this.date, this.serviceType, this.otDescr, this.joNbr, this.otdstart, this.otdend, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'otID': otID,
      'otNbr': otNbr,
      'lineStatus': lineStatus,
      'date': date,
      'serviceType': serviceType,
      'otDescr': otDescr,
      'joNbr': joNbr,
      'otdstart': otdstart,
      'otdend': otdend,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static OvertimeDetailObj fromMap(Map<String, dynamic> map) {
    return OvertimeDetailObj(
      id: map['id'],
      otID: map['otID'],
      otNbr: map['otNbr'],
      lineStatus: map['lineStatus'],
      date: map['date'],
      serviceType: map['serviceType'],
      otDescr: map['otDescr'],
      joNbr: map['joNbr'],
      otdstart: map['otdstart'],
      otdend: map['otdend'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}