import 'package:mob_app/models/model.dart';

class OvertimeObj extends Model {
  static String table = 'overtime';

  int id;
  String custCode;
  String custName;
  String dept;
  String otNbr;
  String reqDate;
  String reqStatus;
  String remarks;
  String lastModifiedDateTime;

  OvertimeObj({this.id, this.custCode, this.custName, this.dept, this.otNbr, this.reqDate, this.reqStatus, this.remarks, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'custCode': custCode,
      'custName': custName,
      'dept': dept,
      'otNbr': otNbr,
      'reqDate': reqDate,
      'reqStatus': reqStatus,
      'remarks': remarks,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static OvertimeObj fromMap(Map<String, dynamic> map) {
    return OvertimeObj(
      id: map['id'],
      custCode: map['custCode'],
      custName: map['custName'],
      dept: map['dept'],
      otNbr: map['otNbr'],
      reqDate: map['reqDate'],
      reqStatus: map['reqStatus'],
      remarks: map['remarks'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}