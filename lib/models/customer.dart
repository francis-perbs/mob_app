import 'package:mob_app/models/model.dart';

class Customer extends Model {
  static String table = 'customer';

  int id;
  String custCode;
  String custName;
  String custEmail;
  String custContactNbr;
  String custAddr;
  String lastModifiedDateTime;

  Customer({this.id, this.custCode, this.custName, this.custEmail, this.custContactNbr, this.custAddr, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String,dynamic> map = {
      'custCode': custCode,
      'custName': custName,
      'custEmail': custEmail,
      'custContactNbr': custContactNbr,
      'custAddr': custAddr,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static Customer fromMap(Map<String, dynamic> map) {
    return Customer(
      id: map['id'],
      custCode: map['custCode'],
      custName: map['custName'],
      custEmail: map['custEmail'],
      custContactNbr: map['custContactNbr'],
      custAddr: map['custAddr'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}