import 'package:mob_app/models/model.dart';

class ExpenseClaimObj extends Model {
  static String table = 'expense_claim';

  int id;
  String requestor;
  String payee;
  String docStatus;
  String ecNbr;
  String ecDate;
  String remarks;
  String lastModifiedDateTime;

  ExpenseClaimObj({this.id, this.requestor, this.payee, this.docStatus, this.ecNbr, this.ecDate, this.remarks, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'requestor': requestor,
      'payee': payee,
      'docStatus': docStatus,
      'ecNbr': ecNbr,
      'ecDate': ecDate,
      'remarks': remarks,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static ExpenseClaimObj fromMap(Map<String, dynamic> map) {
    return ExpenseClaimObj(
      id: map['id'],
      requestor: map['requestor'],
      payee: map['payee'],
      docStatus: map['docStatus'],
      ecNbr: map['ecNbr'],
      ecDate: map['ecDate'],
      remarks: map['remarks'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}