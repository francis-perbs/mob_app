import 'package:mob_app/models/model.dart';

class JobOrderObj extends Model {
  static String table = 'job_order';

  int id;
  int isInternal;
  String custCode;
  String custName;
  String clientRepName;
  String jobOrderNbr;
  String jobOrderDate;
  String jobOrderStatus;
  String refNbr;
  String requesterID;
  String salesOrder;
  String serviceNature;
  String assignedTo;
  String serviceType;
  String descr;
  String remarks;
  String lastModifiedDateTime;

  //List<JobOrderDetailObj> joDetails;

  JobOrderObj({this.id, this.isInternal, this.custCode, this.custName, this.clientRepName, this.jobOrderNbr, this.jobOrderDate, this.jobOrderStatus, this.refNbr, this.requesterID, this.salesOrder, this.serviceNature, this.assignedTo, this.serviceType, this.descr, this.remarks, this.lastModifiedDateTime, /*this.joDetails*/});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'isInternal': isInternal,
      'custCode': custCode,
      'custName': custName,
      'clientRepName': clientRepName,
      'jobOrderNbr': jobOrderNbr,
      'jobOrderDate': jobOrderDate,
      'jobOrderStatus': jobOrderStatus,
      'refNbr': refNbr,
      'requesterID': requesterID,
      'salesOrder': salesOrder,
      'serviceNature': serviceNature,
      'assignedTo': assignedTo,
      'serviceType': serviceType,
      'descr': descr,
      'remarks': remarks,
      'lastModifiedDateTime': lastModifiedDateTime,
      //'joDetails': List<JobOrderDetailObj>.from(joDetails.map((x) => x.toMap())),
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static JobOrderObj fromMap(Map<String, dynamic> map) {
    // final items = map['joDetails'] as List;
    final JobOrderObj jobOrderObj =

    // jobOrderObj = items == null ?
    // JobOrderObj(
    //   id: map['id'],
    //   isInternal: map['isInternal'],
    //   custCode: map['custCode'],
    //   custName: map['custName'],
    //   clientRepName: map['clientRepName'],
    //   jobOrderNbr: map['jobOrderNbr'],
    //   jobOrderDate: map['jobOrderDate'],
    //   jobOrderStatus: map['jobOrderStatus'],
    //   refNbr: map['refNbr'],
    //   requesterID: map['requesterID'],
    //   salesOrder: map['salesOrder'],
    //   serviceNature: map['serviceNature'],
    //   assignedTo: map['assignedTo'],
    //   serviceType: map['serviceType'],
    //   descr: map['descr'],
    //   remarks: map['remarks'],
    //   lastModifiedDateTime: map['lastModifiedDateTime'],
    // ) : 
    JobOrderObj(
      id: map['id'],
      isInternal: map['isInternal'],
      custCode: map['custCode'],
      custName: map['custName'],
      clientRepName: map['clientRepName'],
      jobOrderNbr: map['jobOrderNbr'],
      jobOrderDate: map['jobOrderDate'],
      jobOrderStatus: map['jobOrderStatus'],
      refNbr: map['refNbr'],
      requesterID: map['requesterID'],
      salesOrder: map['salesOrder'],
      serviceNature: map['serviceNature'],
      assignedTo: map['assignedTo'],
      serviceType: map['serviceType'],
      descr: map['descr'],
      remarks: map['remarks'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
      //joDetails: List<JobOrderDetailObj>.from((map['joDetails'] as List).map((x) => JobOrderDetailObj.fromMap(x))),
    );

    return jobOrderObj;
  }

}