import 'package:mob_app/models/model.dart';

class LeaveDetailObj extends Model {
  static String table ='leave_detail';

  int id;
  int leaveID;
  String reqRefNbr;
  String date;
  String leaveDescr;
  String leaveFrom;
  String leaveTo;
  num noOfHrs;
  String lastModifiedDateTime;

  LeaveDetailObj({this.id, this.leaveID, this.reqRefNbr, this.date, this.leaveDescr, this.leaveFrom, this.leaveTo, this.noOfHrs, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'leaveID': leaveID,
      'reqRefNbr': reqRefNbr,
      'date': date,
      'leaveDescr': leaveDescr,
      'leaveFrom': leaveFrom,
      'leaveTo': leaveTo,
      'noOfHrs': noOfHrs,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static LeaveDetailObj fromMap(Map<String, dynamic> map) {
    return LeaveDetailObj(
      id: map['id'],
      leaveID: map['leaveID'],
      reqRefNbr: map['reqRefNbr'],
      date: map['date'],
      leaveDescr: map['leaveDescr'],
      leaveFrom: map['leaveFrom'],
      leaveTo: map['leaveTo'],
      noOfHrs: map['noOfHrs'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}