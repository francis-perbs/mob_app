import 'package:mob_app/models/model.dart';

class LeaveObj extends Model {
  static String table = 'leave';

  int id;
  String empCode;
  String empName;
  String department;
  String reqRefNbr;
  String reqDate;
  String reqStatus;
  String natureLeave;
  num leaveDays;
  int withPay;
  String remarks;
  String lastModifiedDateTime;

  LeaveObj({this.id, this.empCode, this.empName, this.department, this.reqRefNbr, this.reqDate, this.reqStatus, this.natureLeave, this.leaveDays, this.withPay = 0, this.remarks, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'empCode': empCode,
      'empName': empName,
      'department': department,
      'reqRefNbr': reqRefNbr,
      'reqDate': reqDate,
      'reqStatus': reqStatus,
      'natureLeave': natureLeave,
      'leaveDays': leaveDays,
      'withPay': withPay,
      'remarks': remarks,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static LeaveObj fromMap(Map<String, dynamic> map) {
    return LeaveObj(
      id: map['id'],
      empCode: map['empCode'],
      empName: map['empName'],
      department: map['department'],
      reqRefNbr: map['reqRefNbr'],
      reqDate: map['reqDate'],
      reqStatus: map['reqStatus'],
      natureLeave: map['natureLeave'],
      leaveDays: map['leaveDays'],
      withPay: map['withPay'],
      remarks: map['remarks'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}