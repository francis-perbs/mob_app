import 'package:mob_app/models/model.dart';

class JobOrderDetailObj extends Model {
  static String table = 'job_order_detail';

  int id;
  int joID;
  String jobOrderNbr;
  String serviceType;
  String clientName;
  String workDescr;
  String custRep;
  String jodstart;
  String jodend;
  int charge;
  String lastModifiedDateTime;

  JobOrderDetailObj({this.id, this.joID, this.jobOrderNbr, this.serviceType, this.clientName, this.workDescr, this.custRep, this.jodstart, this.jodend, this.charge, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'joID' : joID,
      'jobOrderNbr': jobOrderNbr,
      'serviceType': serviceType,
      'clientName': clientName,
      'workDescr': workDescr,
      'custRep': custRep,
      'jodstart': jodstart,
      'jodend': jodend,
      'charge': charge,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static JobOrderDetailObj fromMap(Map<String, dynamic> map) {
    return JobOrderDetailObj(
      id: map['id'],
      joID: map['joID'],
      jobOrderNbr: map['jobOrderNbr'],
      serviceType: map['serviceType'],
      clientName: map['clientName'],
      workDescr: map['workDescr'],
      custRep: map['custRep'],
      jodstart: map['jodstart'],
      jodend: map['jodend'],
      charge: map['charge'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}