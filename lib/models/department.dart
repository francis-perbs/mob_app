import 'package:mob_app/models/model.dart';

class Department extends Model {
  static String table = 'department';

  int id;
  String deptCode;
  String deptName;
  String deptDescr;
  String lastModifiedDateTime;

  Department({this.id, this.deptCode, this.deptName, this.deptDescr, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'deptCode': deptCode,
      'deptName': deptName,
      'deptDescr': deptDescr,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static Department fromMap(Map<String, dynamic> map) {
    return Department(
      id: map['id'],
      deptCode: map['deptCode'],
      deptName: map['deptName'],
      deptDescr: map['deptDescr'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}