import 'package:mob_app/models/model.dart';

class ExpenseClaimDetailObj extends Model {
  static String table = 'expense_claim_detail';

  int id;
  int ecID;
  String ecNbr;
  String joNbr;
  String joDate;
  String clientName;
  String expenseType;
  String descr;
  num amt;
  String lastModifiedDateTime;

  ExpenseClaimDetailObj({this.id, this.ecID, this.ecNbr, this.joNbr, this.joDate, this.clientName, this.expenseType, this.descr, this.amt, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'ecID': ecID,
      'ecNbr': ecNbr,
      'joNbr': joNbr,
      'joDate': joDate,
      'clientName': clientName,
      'expenseType': expenseType,
      'descr': descr,
      'amt': amt,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static ExpenseClaimDetailObj fromMap(Map<String, dynamic> map) {
    return ExpenseClaimDetailObj(
      id: map['id'],
      ecID: map['ecID'],
      ecNbr: map['ecNbr'],
      joNbr: map['joNbr'],
      joDate: map['joDate'],
      clientName: map['clientName'],
      expenseType: map['expenseType'],
      descr: map['descr'],
      amt: map['amt'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}