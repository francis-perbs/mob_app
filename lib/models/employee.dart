import 'package:mob_app/models/model.dart';

class Employee extends Model {
  static String table = 'employee';

  int id;
  String empCode;
  String empLastName;
  String empGivenName;
  String empMiddleName;
  String empBirthDate;
  String empGender;
  String empEmail;
  String empContactNbr;
  String empAddr;
  String lastModifiedDateTime;

  Employee({this.id, this.empCode, this.empLastName, this.empGivenName, this.empMiddleName, this.empBirthDate, this.empGender, this.empEmail, this.empContactNbr, this.empAddr, this.lastModifiedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'empCode': empCode,
      'empLastName': empLastName,
      'empGivenName': empGivenName,
      'empMiddleName': empMiddleName,
      'empBirthDate': empBirthDate,
      'empGender': empGender,
      'empEmail': empEmail,
      'empContactNbr': empContactNbr,
      'empAddr': empAddr,
      'lastModifiedDateTime': lastModifiedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static Employee fromMap(Map<String, dynamic> map) {
    return Employee(
      id: map['id'],
      empCode: map['empCode'],
      empLastName: map['empLastName'],
      empGivenName: map['empGivenName'],
      empMiddleName: map['empMiddleName'],
      empBirthDate: map['empBirthDate'],
      empGender: map['empGender'],
      empEmail: map['empEmail'],
      empContactNbr: map['empContactNbr'],
      empAddr: map['empAddr'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
    );
  }
}