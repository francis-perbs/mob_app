import 'package:mob_app/models/model.dart';

class JobOrderFileObj extends Model {
  static String table = 'job_order_file';

  int id;
  int joID;
  String jobOrderNbr;
  String imgName;
  String imgPath;
  String lastModifiedDateTime;
  String lastSubmittedDateTime;

  JobOrderFileObj({this.id, this.joID, this.jobOrderNbr, this.imgName, this.imgPath, this.lastModifiedDateTime, this.lastSubmittedDateTime});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'joID': joID,
      'jobOrderNbr': jobOrderNbr,
      'imgName': imgName,
      'imgPath':imgPath,
      'lastModifiedDateTime': lastModifiedDateTime,
      'lastSubmittedDateTime': lastSubmittedDateTime,
    };

    if (id != null) { map['id'] = id; }
    return map;
  }

  static JobOrderFileObj fromMap(Map<String, dynamic> map) {
    return JobOrderFileObj(
      id: map['id'],
      joID: map['joID'],
      jobOrderNbr: map['jobOrderNbr'],
      imgName: map['imgName'],
      imgPath: map['imgPath'],
      lastModifiedDateTime: map['lastModifiedDateTime'],
      lastSubmittedDateTime: map['lastSubmittedDateTime'],
    );
  }
}