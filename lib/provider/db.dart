import 'package:mob_app/models/job_order.dart';
import 'package:mob_app/models/job_order_detail.dart';
import 'package:mob_app/models/job_order_file.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

abstract class DB {
  static Database _db;

  static int get _version => 1;

  static Future<void> init() async {
    if (_db != null) return;

    Future _onConfigure(Database _db) async {
      await _db.execute('PRAGMA foreign_keys = ON');
    }

    void _createTableEmployeev1(Batch batch) {
      batch.execute('drop table if exists employee');
      batch.execute('''
        create table employee (
          id integer primary key autoincrement,
          empCode text,
          empLastName text,
          empGivenName text,
          empMiddleName text,
          empBirthDate text,
          empGender text,
          empEmail text,
          empContactNbr text,
          empAddr text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableCustomerv1(Batch batch) {
      batch.execute('drop table if exists customer');
      batch.execute('''
        create table customer (
          id integer primary key autoincrement,
          custCode text,
          custName text,
          custEmail text,
          custContactNbr text,
          custAddr text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableDepartmentv1(Batch batch) {
      batch.execute('drop table if exists department');
      batch.execute('''
        create table department (
          id integer primary key autoincrement,
          deptCode text,
          deptName text,
          deptDescr text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableJobOrderv1(Batch batch) {
      batch.execute('drop table if exists job_order');
      batch.execute('''
        create table job_order (
          id integer primary key autoincrement,
          isInternal integer,
          custCode text,
          custName text,
          clientRepName text,
          jobOrderNbr text,
          jobOrderDate text,
          jobOrderStatus text,
          refNbr text,
          requesterID text,
          salesOrder text,
          serviceNature text,
          assignedTo text,
          serviceType text,
          descr text,
          remarks text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableLeavev1(Batch batch) {
      batch.execute('drop table if exists leave');
      batch.execute('''
        create table leave (
          id integer primary key autoincrement,
          empCode text,
          empName text,
          department text,
          reqRefNbr text,
          reqDate text,
          reqStatus text,
          natureLeave text,
          leaveDays real,
          withPay integer,
          remarks text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableOvertimev1(Batch batch) {
      batch.execute('drop table if exists overtime');
      batch.execute('''
        create table overtime (
          id integer primary key autoincrement,
          requestor text,
          custCode text,
          custName text,
          dept text,
          otNbr text,
          reqDate text,
          reqStatus text,
          remarks text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableExpenseClaimv1(Batch batch) {
      batch.execute('drop table if exists expense_claim');
      batch.execute('''
        create table expense_claim (
          id integer primary key autoincrement,
          requestor text,
          payee text,
          docStatus text,
          ecNbr text,
          ecDate text,
          remarks text,
          lastModifiedDateTime text
        )
      ''');
    }

    void _createTableJobOrderDetailsv1(Batch batch) {
      batch.execute('drop table if exists job_order_detail');
      batch.execute('''
        create table job_order_detail (
          id integer primary key autoincrement,
          joID integer,
          jobOrderNbr text,
          serviceType text,
          clientName text,
          workDescr text,
          custRep text,
          jodstart text,
          jodend text,
          charge integer,
          lastModifiedDateTime text,
          Foreign Key (joID) references job_order(id) on delete cascade
        )
      ''');
    }

    void _createTableLeaveDetailv1(Batch batch) {
      batch.execute('drop table if exists leave_detail');
      batch.execute('''
        create table leave_detail (
          id integer primary key autoincrement,
          leaveID integer,
          reqRefNbr text,
          date text,
          leaveDescr text,
          leaveFrom text,
          leaveTo text,
          noOfHrs real,
          lastModifiedDateTime text,
          Foreign Key (leaveID) references leave(id) on delete cascade
        )
      ''');
    }

    void _createTableOvertimeDetailv1(Batch batch) {
      batch.execute('drop table if exists overtime_detail');
      batch.execute('''
        create table overtime_detail (
          id integer primary key autoincrement,
          otID integer,
          otNbr text,
          lineStatus text,
          date text,
          serviceType text,
          otDescr text,
          joNbr text,
          otdstart text,
          otdend text,
          lastModifiedDateTime text,
          Foreign Key (otID) references overtime(id) on delete cascade
        )
      ''');
    }

    void _createTableExpenseClaimDetail(Batch batch) {
      batch.execute('drop table if exists expense_claim_detail');
      batch.execute('''
        create table expense_claim_detail (
          id integer primary key autoincrement,
          ecID integer,
          ecNbr text,
          joNbr text,
          joDate text,
          clientName text,
          expenseType text,
          descr text,
          amt real,
          lastModifiedDateTime text,
          Foreign Key (ecID) references expense_claim(id) on delete cascade
        )
      ''');
    }

    void _createTableJobOrderFile(Batch batch) {
      batch.execute('drop table if exists job_order_file');
      batch.execute('''
        create table job_order_file (
          id integer primary key autoincrement,
          joID int,
          jobOrderNbr text,
          imgName text,
          imgPath text,
          lastModifiedDateTime text,
          lastSubmittedDateTime text,
          Foreign Key (joID) references job_order(id) on delete cascade
        )
      ''');
    }

    void _deleteAllTables(Batch batch) {
      batch.execute('drop table if exists employee');
      batch.execute('drop table if exists customer');
      batch.execute('drop table if exists department');
      batch.execute('drop table if exists job_order');
      batch.execute('drop table if exists leave');
      batch.execute('drop table if exists overtime');
      batch.execute('drop table if exists expense_claim');
      batch.execute('drop table if exists job_order_detail');
      batch.execute('drop table if exists leave_detail');
      batch.execute('drop table if exists overtime_detail');
      batch.execute('drop table if exists expense_claim_detail');
      batch.execute('drop table if exists job_order_file');
    }

    try {
      String _path = await getDatabasesPath() + 'mob_app.db';

      _db = await openDatabase(_path,
        version: _version,
        onConfigure: _onConfigure,
        onCreate: (db, version) async {
          var batch = db.batch();
          _createTableEmployeev1(batch);
          _createTableCustomerv1(batch);
          _createTableDepartmentv1(batch);
          _createTableJobOrderv1(batch);
          _createTableLeavev1(batch);
          _createTableOvertimev1(batch);
          _createTableExpenseClaimv1(batch);
          _createTableJobOrderDetailsv1(batch);
          _createTableLeaveDetailv1(batch);
          _createTableOvertimeDetailv1(batch);
          _createTableExpenseClaimDetail(batch);
          _createTableJobOrderFile(batch);
          await batch.commit();
        },
        onUpgrade: (db, oldV, newV) async {
          var batch = db.batch();
          _deleteAllTables(batch);
          await batch.commit();
        },
        onDowngrade: onDatabaseDowngradeDelete,
      );
    } catch(ex) {
      print(ex);
    }
  }

  static Future<List<Map<String, dynamic>>> query(String table) async => _db.query(table);

  static Future<List<Map<String, dynamic>>> queryOneByID(String table, int id) async => await _db.query(table, where: 'id = ?', whereArgs: [id]);

  static Future<List<Map<String, dynamic>>> queryOneByLastModifiedDateTime(String table, String modifiedDateTime) async => await _db.query(table, where: 'lastModifiedDateTime like ?', whereArgs: [modifiedDateTime]);


  static Future<int> insertJobOrder(String table, JobOrderObj model) async => await _db.insert(table, model.toMap());

  static Future<int> updateJobOrder(String table, JobOrderObj model) async => await _db.update(table, model.toMap(), where: 'id = ?', whereArgs: [model.id]);

  static Future<int> deleteJobOrder(String table, JobOrderObj model) async => await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);


  static Future<List<Map<String, dynamic>>> queryJobOrderDetailsByParentID(String table, int key) async => _db.query(table, where: 'joID = ?', whereArgs: [key]);

  static Future<int> insertJobOrderDetail(String table, JobOrderDetailObj model) async => await _db.insert(table, model.toMap());

  static Future<int> updateJobOrderDetail(String table, JobOrderDetailObj model) async => await _db.update(table, model.toMap(), where: 'id = ?', whereArgs: [model.id]);

  static Future<int> deleteJobOrderDetail(String table, JobOrderDetailObj model) async => await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);


  static Future<List<Map<String, dynamic>>> queryJobOrderFilesByParentID(String table, int key) async => _db.query(table, where: 'joID = ?', whereArgs: [key]);

  static Future<int> insertJobOrderFile(String table, JobOrderFileObj model) async => await _db.insert(table, model.toMap());

  static Future<int> deleteJobOrderFile(String table, JobOrderFileObj model) async => await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);

}