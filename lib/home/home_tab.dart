import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/claim/claim.dart';
import 'package:mob_app/home/claim/claim_detail.dart';
import 'package:mob_app/home/job_order/job_order.dart';
import 'package:mob_app/home/job_order/job_order_list.dart';
import 'package:mob_app/store/attach_file_store.dart';
import 'package:mob_app/store/job_order_detail_store.dart';
import 'package:mob_app/store/job_order_store.dart';
import 'package:mob_app/home/leave_app/leave_app.dart';
import 'package:mob_app/home/leave_app/leave_app_detail.dart';
import 'package:mob_app/home/overtime/overtime.dart';
import 'package:mob_app/home/overtime/overtime_detail.dart';
import 'package:mob_app/models/job_order.dart';
import 'package:provider/provider.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key key}) : super(key: key);

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  int _currentTabIndex = 0;

  final JobOrderStore jobOrderStore = JobOrderStore();
  final AttachFileStore attachFileStore = AttachFileStore();

  final _appBarHeader = <Widget>[
    Text('Job Order'),
    Text('Leave'),
    Text('Overtime'),
    Text('Expense Claim'),
  ];

  final _bottomNavBarItems = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      backgroundColor: Colors.blue,
      icon: Icon(Icons.assignment),
      title: Text('Job Order'),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.blue,
      icon: Icon(Icons.date_range),
      title: Text('Leave'),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.blue,
      icon: Icon(Icons.book),
      title: Text('Overtime'),
    ),
    BottomNavigationBarItem(
      backgroundColor: Colors.blue,
      icon: Icon(Icons.attach_money),
      title: Text('Expense Claim'),
    ),
  ];

  final _bottomNavBarPages = <Widget>[
    JobOrderList(),
    Center(
      child: Text('Leave List'),
    ),
    Center(
      child: Text('Overtime List'),
    ),
    Center(
      child: Text('Expense Claim List'),
    ),
  ];

  @override
  void initState() {
    super.initState();
    jobOrderStore.fetchJobOrders();
  }

  @override
  Widget build(BuildContext context) => MultiProvider(
    providers: [
      Provider<JobOrderStore>(create: (_) => jobOrderStore),
      Provider<AttachFileStore>(create: (_) => attachFileStore),
    ],
    child: Observer(
      builder: (_) =>
      Scaffold(
        appBar: AppBar(
                title: _appBarHeader[_currentTabIndex],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Text('About'),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                title: Text('Account'),
                onTap: () {
                  Navigator.pushNamed(context, '/about');
                },
              ),
              ListTile(
                title: Text('Logout'),
                onTap: () {
                  Navigator.pushNamedAndRemoveUntil(context, '/logout', (Route<dynamic> route) => false);
                  //Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
        body: _bottomNavBarPages[_currentTabIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: _bottomNavBarItems,
          currentIndex: _currentTabIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Theme.of(context).accentColor,
          selectedItemColor: Colors.white,
          onTap: (int index) {
            setState(() => this._currentTabIndex = index);
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            switch (this._currentTabIndex) {
              case 0:
                jobOrderStore.jobOrderSaveAction = 'insert';
                jobOrderStore.resetJobOrder();
                jobOrderStore.resetJobOrderDetailObservable();
                jobOrderStore.resetSelectedJobOrderDetails();
                jobOrderStore.resetJobOrderFilesObservable();
                jobOrderStore.jobOrder.jobOrderDate = DateFormat('dd/MM/yyyy').format(DateTime.now());
                jobOrderStore.jobOrder.isInternal = 0;
                //jobOrderStore.jobOrder.joDetails = [];
                //fix new job order bug
                Navigator.push(context, 
                  MaterialPageRoute(
                    builder: (context) => JobOrder(appTitle: 'New Job Order', jobOrderStore: jobOrderStore,),
                  ),
                );
                break;
              case 1:
                Navigator.push(context, 
                  MaterialPageRoute(
                    builder: (context) => /*LeaveApp(appTitle: 'New Leave Application')*/ LeaveAppDetail(appTitle: 'New Leave Detail')
                  ),
                );
                break;
              case 2:
                Navigator.push(context, 
                  MaterialPageRoute(
                    builder: (context) => /*Overtime(appTitle: 'New Overtime Application')*/ OvertimeDetail(appTitle: 'New Overtime Detail')
                  ),
                );
                break;
              case 3:
                Navigator.push(context, 
                  MaterialPageRoute(
                    builder: (context) => /*ExpenseClaim(appTitle: 'New Expense Claim')*/ ExpenseClaimDetail(appTitle: 'New Expense Claim Detail')
                  ),
                );
                break;
            }
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.green,
        ),
    ),
    ),
  );
}