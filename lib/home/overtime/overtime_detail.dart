import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

class OvertimeDetail extends StatefulWidget {
  final String appTitle;

  OvertimeDetail({Key key, this.appTitle}) : super(key: key);

  @override
  _OvertimeDetailState createState() => _OvertimeDetailState(this.appTitle);
}

class _OvertimeDetailState extends State<OvertimeDetail> {
  String appTitle;
  _OvertimeDetailState(this.appTitle);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: 'Save Record',
            onPressed: () {
              return null;
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            initialValue: {
              'ot_detail_date': DateTime.now(),
              'ot_detail_start': DateTime.now(),
              'ot_detail_end': DateTime.now(),
            },
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black54,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    child: Column(
                      children: <Widget>[
                        FormBuilderTextField(
                          attribute: 'ot_detail_status',
                          decoration: InputDecoration(labelText: 'Status'),
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'ot_detail_date',
                          inputType: InputType.date,
                          format: DateFormat.yMd(),
                          decoration: InputDecoration(labelText: 'Date'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ot_detail_service_type',
                          decoration: InputDecoration(labelText: 'Service Type'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ot_detail_descr',
                          decoration: InputDecoration(labelText: 'Overtime Description'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ot_detail_jo_nbr',
                          decoration: InputDecoration(labelText: 'Job Order Nbr'),
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'ot_detail_start',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'Start'),
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'ot_detail_end',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'End'),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                return null;
              },
              child: Text('Press Me'),
            ),
          ),
        ],
      ),
    );
  }
}