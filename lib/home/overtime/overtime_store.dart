import 'package:mobx/mobx.dart';

part 'overtime_store.g.dart';

class OvertimeStore = _OvertimeStore with _$OvertimeStore;

abstract class _OvertimeStore with Store {
  @observable
  bool expandDescrPanel = true;

  @observable
  bool expandInfoPanel = false;

  @observable
  bool expandRemarksPanel = false;
}