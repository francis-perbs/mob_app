import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/overtime/overtime_store.dart';
import 'package:provider/provider.dart';

class ExpansionPanelOvertimeDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overtimeStore = Provider.of<OvertimeStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        overtimeStore.expandDescrPanel = !overtimeStore.expandDescrPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: overtimeStore.expandDescrPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Description'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderDropdown(
                  attribute: 'ot_cust_code',
                  decoration: InputDecoration(
                    labelText: "Customer Code",
                    //errorText:
                  ),
                  initialValue: '',
                  items: ['','CUST001', 'CUST002'].map(
                    (item) => DropdownMenuItem(
                      value: item,
                      child: Text('$item'))
                  ).toList(),
                  onChanged: (val) {
                    //jobOrderStore.customerCode = val;
                    //jobOrderStore.validateCustomerCode(val);
                    return null;
                  },
                ),
                FormBuilderTextField(
                  attribute: 'ot_cust_name',
                  decoration: InputDecoration(labelText: 'Customer Name'),
                  readOnly: true,
                ),
                FormBuilderTextField(
                  attribute: 'ot_emp_dept',
                  decoration: InputDecoration(labelText: 'Department'),
                  readOnly: true,
                ),
                FormBuilderTextField(
                  attribute: 'ot_ref_nbr',
                  decoration: InputDecoration(labelText: 'Ref. Nbr.'),
                  readOnly: true,
                ),
                FormBuilderDateTimePicker(
                  attribute: 'ot_date',
                  inputType: InputType.date,
                  format: DateFormat('dd/MM/yyyy'),
                  decoration: InputDecoration(labelText: 'Overtime Date'),
                ),
                FormBuilderTextField(
                  attribute: 'ot_status',
                  decoration: InputDecoration(labelText: 'Overtime Status'),
                  initialValue: 'Saved',
                  readOnly: true,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelOvertimeInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overtimeStore = Provider.of<OvertimeStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        overtimeStore.expandInfoPanel = !overtimeStore.expandInfoPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: overtimeStore.expandInfoPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Information'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            height: 300.0,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columns: [
                        DataColumn(label: Text('Status')),
                        DataColumn(label: Text('Date')),
                        DataColumn(label: Text('Service Type')),
                        DataColumn(label: Text('Description')),
                        DataColumn(label: Text('JO Number')),
                        DataColumn(label: Text('Start')),
                        DataColumn(label: Text('End')),
                      ],
                      rows: [
                        DataRow(
                          cells: [
                            DataCell(Text('Pending')),
                            DataCell(Text('01/05/2020')),
                            DataCell(Text('Development')),
                            DataCell(Text('Development Overtime')),
                            DataCell(Text('JO001')),
                            DataCell(Text('6:30 PM')),
                            DataCell(Text('9:00 PM')),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelOvertimeRemarks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final overtimeStore = Provider.of<OvertimeStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        overtimeStore.expandRemarksPanel = !overtimeStore.expandRemarksPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: overtimeStore.expandRemarksPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Remarks'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderTextField(
                  attribute: 'ot_remarks',
                  decoration: InputDecoration(labelText: 'Remarks'),
                  minLines: 3,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}