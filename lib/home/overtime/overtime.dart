import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mob_app/home/overtime/overtime_expansion.dart';
import 'package:mob_app/home/overtime/overtime_store.dart';
import 'package:provider/provider.dart';

class Overtime extends StatefulWidget {
  final String appTitle;

  Overtime({Key key, this.appTitle}) : super(key: key);

  @override
  _OvertimeState createState() => _OvertimeState(this.appTitle);
}

class _OvertimeState extends State<Overtime> {
  String appTitle;
  _OvertimeState(this.appTitle);

  final OvertimeStore overtimeStore = OvertimeStore();

  @override
  Widget build(BuildContext context) => Provider<OvertimeStore>(
    create: (_) => overtimeStore,
    child: Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: null
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            initialValue: {
              'ot_date': DateTime.now(),
            },
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  ExpansionPanelOvertimeDescription(),
                  ExpansionPanelOvertimeInformation(),
                  ExpansionPanelOvertimeRemarks(),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}