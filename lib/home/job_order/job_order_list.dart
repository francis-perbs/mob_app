import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mob_app/home/job_order/job_order.dart';
import 'package:mob_app/store/attach_file_store.dart';
import 'package:mob_app/store/job_order_detail_store.dart';
import 'package:mob_app/store/job_order_store.dart';
import 'package:provider/provider.dart';

class JobOrderList extends StatelessWidget {

  //final JobOrderStore jobOrderStore = JobOrderStore();

  @override
  Widget build(BuildContext context) {
    final jobOrderStore = Provider.of<JobOrderStore>(context);
    final attachFileStore = Provider.of<AttachFileStore>(context);
    
    jobOrderStore.fetchJobOrders();

    return Container(
      child: Observer(
        builder: (_) {
          if (jobOrderStore.jobOrdersObservable.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text('Job Order List'),
                ),
              ],
            );
          }
          
          return RefreshIndicator(
            child: ListView.builder(
            itemCount: jobOrderStore.jobOrders.length,
            itemBuilder: (_, int index) {
              final _jobOrder = jobOrderStore.jobOrders[index];
              return Dismissible(
                key: Key(_jobOrder.id.toString()),
                child: ListTile(
                  leading: Icon(Icons.event_available),
                  title: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text('${_jobOrder.id}: ${_jobOrder.jobOrderNbr}'),
                      ),
                    ],
                  ),
                  subtitle: Text(_jobOrder.descr ?? ''),
                  onTap: () {
                    jobOrderStore.jobOrderSaveAction = 'update';

                    jobOrderStore.resetJobOrder();
                    jobOrderStore.resetJobOrderDetailObservable();
                    jobOrderStore.resetSelectedJobOrderDetails();
                    jobOrderStore.resetJobOrderFilesObservable();
                    
                    jobOrderStore.jobOrder = _jobOrder;
                    jobOrderStore.fetchJobOrderDetails();
                    jobOrderStore.fetchJobOrderFiles();
                    //jobOrderStore.jobOrder.joDetails = [];
                    Navigator.push(context, 
                      MaterialPageRoute(
                        builder: (context) => JobOrder(appTitle: 'View Job Order', jobOrderStore: jobOrderStore,),
                      ),
                    );
                  },
                  // trailing: IconButton(
                  //   icon: Icon(Icons.delete),
                  //   tooltip: 'Delete Record',
                  //   onPressed: () {
                  //     jobOrderStore.deleteJobOrder(_jobOrder);
                  //     jobOrderStore.fetchJobOrders();
                  //   },
                  // ),
                ),
                direction: DismissDirection.startToEnd,
                onDismissed: (direction) {
                  if (direction == DismissDirection.startToEnd) {
                    jobOrderStore.jobOrder = _jobOrder;
                    jobOrderStore.fetchJobOrderFiles();

                    jobOrderStore.jobOrderFilesObservable.forEach(
                      (joFile) {
                        attachFileStore.jobOrderFile = joFile;
                        attachFileStore.image = null;
                        attachFileStore.image = File(attachFileStore.filePath);
                        attachFileStore.image.delete();
                        attachFileStore.deleteJobOrderFile(joFile);
                        jobOrderStore.jobOrderFilesObservable.remove(joFile);
                        jobOrderStore.jobOrderFiles.remove(joFile);
                      },
                    );

                    jobOrderStore.deleteJobOrder(_jobOrder);
                    jobOrderStore.jobOrders.remove(_jobOrder);
                    jobOrderStore.jobOrdersObservable.remove(_jobOrder);

                    //jobOrderStore.fetchJobOrders();
                  }
                },
              );
            },
          ),
          onRefresh: () => jobOrderStore.fetchJobOrders(),
          );
        },
      ),
    );
  }

}