import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/job_order/job_order_detail.dart';
import 'package:mob_app/store/attach_file_store.dart';
import 'package:mob_app/store/job_order_detail_store.dart';
import 'package:mob_app/store/job_order_store.dart';
import 'package:mob_app/models/job_order_detail.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';
import 'package:zoom_widget/zoom_widget.dart';

class ExpansionPanelReference extends StatelessWidget {
  const ExpansionPanelReference(this.jobOrderStore);

  final JobOrderStore jobOrderStore;

  @override
  Widget build(BuildContext context) {
    //final jobOrderStore = Provider.of<JobOrderStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        jobOrderStore.expandRefPanel = !jobOrderStore.expandRefPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: jobOrderStore.expandRefPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10),
              child: Text('Reference'),
            );
          },
          body:
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
            children: <Widget>[
              FormBuilderSwitch(
                attribute: 'internal_activity_switch',
                label: Text(
                  'Internal Activity',
                  textAlign: TextAlign.center,
                ),
                initialValue: jobOrderStore.jobOrder.isInternal == 1 ? true : false,
                onChanged: (val) {
                  switch (val) {
                    case true:
                      jobOrderStore.jobOrder.isInternal = 1;
                      break;
                    case false:
                      jobOrderStore.jobOrder.isInternal = 0;
                      break;
                  }
                },
              ),
              FormBuilderDropdown(
                attribute: 'customer_code',
                decoration: InputDecoration(
                  labelText: "Cust. Code",
                  errorText: jobOrderStore.jobOrderError.custCode,
                ),
                initialValue: jobOrderStore.jobOrder.custCode,
                items: ['','AAVENDOR', 'APCONSULTING'].map(
                  (item) => DropdownMenuItem(
                    value: item,
                    child: Text('$item'))
                ).toList(),
                onChanged: (val) {
                  jobOrderStore.jobOrder.custCode = val;
                  jobOrderStore.validateCustomerCode(jobOrderStore.jobOrder.custCode);
                },
                //readOnly: jobOrderStore.jobOrder.joDetails != null ? (jobOrderStore.jobOrder.joDetails.isEmpty == true ? false: true) : false,
                readOnly: jobOrderStore.jobOrderDetailsObservable.length == 0 ? false : true,
              ),
              FormBuilderTextField(
                attribute: 'customer_name',
                decoration: InputDecoration(labelText: 'Cust. Name'),
                initialValue: jobOrderStore.jobOrder.custName,
                readOnly: true,
              ),
              FormBuilderTextField(
                attribute: 'job_order_number',
                decoration: InputDecoration(
                  labelText: 'Job Order No.',
                  errorText: jobOrderStore.jobOrderError.jobOrderNbr,
                ),
                initialValue: jobOrderStore.jobOrder.jobOrderNbr,
                onChanged: (val) {
                  jobOrderStore.jobOrder.jobOrderNbr = val;
                  jobOrderStore.validateJobOrderNbr(jobOrderStore.jobOrder.jobOrderNbr);
                },
              ),
              FormBuilderDateTimePicker(
                attribute: 'job_order_date',
                inputType: InputType.date,
                format: DateFormat('dd/MM/yyyy'),
                decoration: InputDecoration(
                  labelText: 'Job Order Date',
                  errorText: jobOrderStore.jobOrderError.jobOrderDate,
                ),
                initialValue: isNull(jobOrderStore.jobOrder.jobOrderDate) ? DateTime.now() : DateFormat('dd/MM/yyyy').parse(jobOrderStore.jobOrder.jobOrderDate),
                onChanged: (val) {
                  //print(val);
                  if (val == null) jobOrderStore.jobOrder.jobOrderDate = null;
                  else jobOrderStore.jobOrder.jobOrderDate = DateFormat('dd/MM/yyyy').format(val);
                  jobOrderStore.validateJobOrderDate(jobOrderStore.jobOrder.jobOrderDate);
                },
              ),
              FormBuilderTextField(
                attribute: 'job_order_status',
                decoration: InputDecoration(labelText: 'Job Order Status'),
                initialValue: jobOrderStore.jobOrder.jobOrderStatus,
                //readOnly: true,
                onChanged: (val) {
                  jobOrderStore.jobOrder.jobOrderStatus = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'ref_nbr',
                decoration: InputDecoration(labelText: 'Ref. Nbr.'),
                initialValue: jobOrderStore.jobOrder.refNbr,
                onChanged: (val) {
                  jobOrderStore.jobOrder.refNbr = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'requester_id',
                decoration: InputDecoration(
                  labelText: 'Requester',
                  errorText: jobOrderStore.jobOrderError.requesterID,
                ),
                initialValue: jobOrderStore.jobOrder.requesterID,
                onChanged: (val) {
                  jobOrderStore.jobOrder.requesterID = val;
                  jobOrderStore.validateRequesterID(jobOrderStore.jobOrder.requesterID);
                },
              ),
              FormBuilderTextField(
                attribute: 'sales_order',
                decoration: InputDecoration(
                  labelText: 'Sales Order',
                  errorText: jobOrderStore.jobOrder.salesOrder,
                ),
                initialValue: jobOrderStore.jobOrder.salesOrder,
                onChanged: (val) {
                  jobOrderStore.jobOrder.assignedTo = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'assign_to',
                decoration: InputDecoration(labelText: 'Assign To'),
                initialValue: jobOrderStore.jobOrder.assignedTo,
                onChanged: (val) {
                  jobOrderStore.jobOrder.assignedTo = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'service_nature',
                decoration: InputDecoration(labelText: 'Service Nature'),
                initialValue: jobOrderStore.jobOrder.serviceNature,
                onChanged: (val) {
                  jobOrderStore.jobOrder.serviceNature = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'service_type',
                decoration: InputDecoration(labelText: 'Service Type'),
                initialValue: jobOrderStore.jobOrder.serviceType,
                onChanged: (val) {
                  jobOrderStore.jobOrder.serviceType = val;
                },
              ),
              FormBuilderTextField(
                attribute: 'descr',
                decoration: InputDecoration(labelText: 'Description'),
                minLines: 3,
                initialValue: jobOrderStore.jobOrder.descr,
                onChanged: (val) {
                  jobOrderStore.jobOrder.descr = val;
                },
              ),
            ]
          ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelFiles extends StatelessWidget {
  const ExpansionPanelFiles(this.jobOrderStore);

  final JobOrderStore jobOrderStore;

  @override
  Widget build(BuildContext context) {
    //final jobOrderStore = Provider.of<JobOrderStore>(context);
    //jobOrderStore.fetchJobOrderFiles();
    final attachFileStore = AttachFileStore();

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
        expansionCallback: (int item, bool status) {
          jobOrderStore.expandFilePanel = !jobOrderStore.expandFilePanel;
        },
        children: [
        ExpansionPanel(
          isExpanded: jobOrderStore.expandFilePanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Files'),
            );
          },
          body: 
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    RaisedButton(
                      padding: EdgeInsets.symmetric(horizontal: 50.0),
                      color: Colors.blue,
                      child: Text('Camera'),
                      onPressed: jobOrderStore.jobOrder.id == null ? null : () async {
                        //var image = await ImagePicker.pickImage(source: ImageSource.gallery);
                        //_image = image;
                      },
                    ),
                    RaisedButton(
                      padding: EdgeInsets.symmetric(horizontal: 50.0),
                      color: Colors.blue,
                      child: Text('Gallery'),
                      onPressed: jobOrderStore.jobOrder.id == null ? null : () async {
                        attachFileStore.resetJobOrderFile();

                        var image = await ImagePicker.pickImage(source: ImageSource.gallery);
                        attachFileStore.image = image;
                        print(image.path);

                        attachFileStore.jobOrderFile.joID = jobOrderStore.jobOrder.id;

                        Directory appDocDir = await getApplicationDocumentsDirectory();
                        attachFileStore.jobOrderFile.imgPath = appDocDir.path;
                        attachFileStore.jobOrderFile.imgName = basename(image.path);

                        attachFileStore.image = await attachFileStore.saveJobOrderFileToApp();
                        print(attachFileStore.filePath);

                        DateTime lastModifiedDateTime = DateTime.now();
                        String lastModifiedDateTimeStr = DateFormat.yMd().add_jm().format(lastModifiedDateTime);
                        attachFileStore.setJobOrderFileLastModifiedDateTime(lastModifiedDateTimeStr);

                        attachFileStore.insertJobOrderFile();
                        
                        //attachFileStore.fetchJobOrderFileByLastModifiedDateTime(lastModifiedDateTimeStr);

                        jobOrderStore.fetchJobOrderFiles();
                      },
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black54,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                height: 300,
                width: MediaQuery.of(context).size.width - 90.0,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 1.0,
                    mainAxisSpacing: 1.0,
                  ),
                  itemCount: jobOrderStore.jobOrderFilesObservable.isEmpty == true ? 0 : jobOrderStore.jobOrderFilesObservable.length,
                  itemBuilder: (context, index) {
                  //crossAxisSpacing: 1.0,
                  //mainAxisSpacing: 1.0,
                  //crossAxisCount: 2,
                  //children: List.generate(6,
                  //(index) {
                    attachFileStore.jobOrderFile = jobOrderStore.jobOrderFilesObservable[index];
                    attachFileStore.image = File(attachFileStore.filePath);

                    return Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                          width: 1.0,
                        ),
                      ),
                      child: GridTile(
                            child: GestureDetector(
                              child: Hero(
                                tag: attachFileStore.filePath,
                                child: Image.file(
                                  attachFileStore.image,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              onTap: () {
                                Navigator.push(context,
                                  MaterialPageRoute(builder: (context) =>
                                    Scaffold(
                                      appBar: AppBar(
                                        title: Text('View Image'),
                                      ),
                                      body: Container(
                                        color: Colors.black54,
                                        child: Center(
                                          child: Zoom(
                                            width: 1000,
                                            height: 1000,
                                            initZoom: 0.4,
                                            backgroundColor: Colors.black54,
                                            child: Hero(
                                              tag: attachFileStore.filePath,
                                              child: Image.file(
                                                attachFileStore.image,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            footer: Center(
                              child: FlatButton(
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                  size: 30.0,
                                ),
                                onPressed: () {
                                  attachFileStore.image.delete();
                                  attachFileStore.deleteJobOrderFile(attachFileStore.jobOrderFile);
                                  jobOrderStore.jobOrderFilesObservable.remove(attachFileStore.jobOrderFile);
                                  jobOrderStore.jobOrderFiles.remove(attachFileStore.jobOrderFile);

                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Image Deleted'),
                                    ),
                                  );
                                },
                              ),
                            ),
                      ),
                    );
                  }
                  //),
                ),
                  //Column(
                  //children: <Widget>[
                      
                      // ListTile(
                      //   title: Text('Image 2'),
                      //   subtitle: Text('Url: http://www.google.com'),
                      // ),
                      // ListTile(
                      //   title: Text('Image 3'),
                      //   subtitle: Text('Url: http://www.google.com'),
                      // ),
                  //],
                //),
              ),
            ],
          ),
        ),
        ],
      ),
    );
  }
}

class ExpansionPanelDetails extends StatelessWidget {
  const ExpansionPanelDetails(this.jobOrderStore);

  final JobOrderStore jobOrderStore;

  @override
  Widget build(BuildContext context) {
    //final jobOrderStore = Provider.of<JobOrderStore>(context);
    //jobOrderStore.fetchJobOrderDetails();
    jobOrderStore.resetSelectedJobOrderDetails();

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
        expansionCallback: (int item, bool status) {
          jobOrderStore.expandDetailsPanel = !jobOrderStore.expandDetailsPanel;
        },
      children: [
        ExpansionPanel(
          isExpanded: jobOrderStore.expandDetailsPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10),
              child: Text('Details'),
            );
          },
          body: Column(
            children: <Widget>[
              Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child:
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 50.0),
                    color: Colors.blue,
                    child: Text('Add'),
                    onPressed: jobOrderStore.jobOrder.id == null ? null :
                    () {
                      final JobOrderDetailStore jobOrderDetailStore = JobOrderDetailStore();
                      jobOrderDetailStore.jobOrderDetailSaveAction = 'insert';
                      //jobOrderDetailStore.resetJobOrderDetail();
                      jobOrderDetailStore.jobOrderDetail.joID = jobOrderStore.jobOrder.id;
                      jobOrderDetailStore.jobOrderDetail.clientName = jobOrderStore.jobOrder.custCode;
                      jobOrderDetailStore.jobOrderDetail.charge = 0;
                      jobOrderDetailStore.jobOrderDetail.jodstart = DateFormat.jm().format(DateTime.now());
                      jobOrderDetailStore.jobOrderDetail.jodend = DateFormat.jm().format(DateTime.now());
                      Navigator.push(context, 
                        MaterialPageRoute(
                          builder: (context) => JobOrderDetail(appTitle: 'New Job Order Detail', jobOrderDetailStore: jobOrderDetailStore, jobOrderStore: jobOrderStore,),
                        ),
                      );
                    },
                  ),
                  RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 50.0),
                    color: Colors.red,
                    child: Text('Delete'),
                    onPressed: jobOrderStore.selectedJobOrderDetails.length == 0 ? null :
                      () {
                        jobOrderStore.selectedJobOrderDetails.forEach(
                          (item) {
                            print(item.id);
                            jobOrderStore.deleteJobOrderDetail(item);
                            jobOrderStore.jobOrderDetailsObservable.remove(item);
                            jobOrderStore.jobOrderDetails.remove(item);
                            //jobOrderStore.jobOrder.joDetails.remove(item);
                          }
                        );
                        jobOrderStore.resetSelectedJobOrderDetails();
                        //recheck this one. this seems to be bug during delete
                      },
                  ),
                ],
              ),
              ),
              Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            height: 300.0,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child:  Column(
              children: <Widget>[
              //
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  columns: [
                    DataColumn(label: Text('Service Type')),
                    DataColumn(label: Text('Client Name')),
                    DataColumn(label: Text('Work Description')),
                    DataColumn(label: Text('Cust. Rep.')),
                    DataColumn(label: Text('Sart')),
                    DataColumn(label: Text('End')),
                    DataColumn(label: Text('Charge')),
                  ],
                  // rows: [
                  //   DataRow(
                  //     cells: [
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //       DataCell(Text('')),
                  //     ],
                  //   ),
                  // ],
                  rows: jobOrderStore.jobOrderDetailsObservable.map(
                    (item) =>
                    DataRow(
                      selected: jobOrderStore.selectedJobOrderDetails.contains(item),
                      onSelectChanged: (bool val) { 
                        if (val) jobOrderStore.selectedJobOrderDetails.add(item);
                        else jobOrderStore.selectedJobOrderDetails.remove(item);
                      },
                      cells: [
                        DataCell(
                          Text(isNull(item.serviceType) ? '' : item.serviceType),
                          onTap: () {
                            final JobOrderDetailStore jobOrderDetailStore = JobOrderDetailStore();
                            jobOrderDetailStore.jobOrderDetailSaveAction = 'update';
                            jobOrderDetailStore.resetJobOrderDetail();
                            jobOrderDetailStore.jobOrderDetail = item;
                            Navigator.push(context, 
                              MaterialPageRoute(
                                builder: (context) => JobOrderDetail(appTitle: 'View Job Order Detail', jobOrderDetailStore: jobOrderDetailStore,),
                              ),
                            );
                          },
                        ),
                        DataCell(
                          Text(isNull(item.clientName) ? '' : item.clientName),
                        ),
                        DataCell(
                          Text(isNull(item.workDescr) ? '' : item.workDescr),
                        ),
                        DataCell(
                          Text(isNull(item.custRep) ? '' : item.custRep),
                        ),
                        DataCell(
                          Text(item.jodstart),
                        ),
                        DataCell(
                          Text(item.jodend),
                        ),
                        DataCell(
                          Text(item.charge == 1 ? 'Yes' : 'No'),
                        ),
                      ]
                    )
                  ).toList(),
                ),
              ),
            ]
          ),
          ),
        ),
          ],
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelRemarks extends StatelessWidget {
  const ExpansionPanelRemarks(this.jobOrderStore);

  final JobOrderStore jobOrderStore;

  @override
  Widget build(BuildContext context) {
    //final jobOrderStore = Provider.of<JobOrderStore>(context);

    return Observer(builder: (_) =>
      ExpansionPanelList(
        expansionCallback: (int item, bool status) {
          jobOrderStore.expandRemarksPanel = !jobOrderStore.expandRemarksPanel;
        },
        children: [
          ExpansionPanel(
            isExpanded: jobOrderStore.expandRemarksPanel,
            headerBuilder: (BuildContext context, bool isExpanded) {
              return Container(
                padding: EdgeInsets.all(10),
                child: Text('Remarks'),
              );
            },
            body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
            children: <Widget>[
              FormBuilderTextField(
                attribute: 'remarks',
                decoration: InputDecoration(labelText: 'Remarks'),
                minLines: 3,
                initialValue: jobOrderStore.jobOrder.remarks,
                onChanged: (val) {
                  jobOrderStore.jobOrder.remarks = val;
                },
              ),
            ]
          ),
        ),
        ),
      ],
      ),
    );
  }
}