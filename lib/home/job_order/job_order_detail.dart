import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/store/job_order_detail_store.dart';
import 'package:mob_app/store/job_order_store.dart';
import 'package:validators/validators.dart';

class JobOrderDetail extends StatefulWidget {
  final String appTitle;
  final JobOrderDetailStore jobOrderDetailStore;
  final JobOrderStore jobOrderStore;

  JobOrderDetail({Key key, this.appTitle, this.jobOrderDetailStore, this.jobOrderStore}) : super(key: key);

  @override
  _JobOrderDetailState createState() => _JobOrderDetailState();
}

class _JobOrderDetailState extends State<JobOrderDetail> {
  final GlobalKey<ScaffoldState> _scaffoldJODKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    widget.jobOrderDetailStore.setupValidations();
  }

  @override
  void dispose() {
    widget.jobOrderDetailStore.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) =>
      Scaffold(
      key: _scaffoldJODKey,
      appBar: AppBar(
        title: Text('${widget.appTitle}'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: 'Save Record',
            onPressed: () {
              if (!widget.jobOrderDetailStore.hasJobOrderDetailValidationError) {
                switch (widget.jobOrderDetailStore.jobOrderDetailSaveAction) {
                  case 'insert':
                    DateTime lastEditDateTime = DateTime.now();
                    String lastEditDateTimeStr = DateFormat.yMd().add_jm().format(lastEditDateTime);
                    widget.jobOrderDetailStore.setJobOrderDetailLastModifiedDateTime(lastEditDateTimeStr);

                    widget.jobOrderDetailStore.insertJobOrderDetail();

                    _scaffoldJODKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Record Saved'),
                      ),
                    );

                    widget.jobOrderDetailStore.jobOrderDetailSaveAction = 'update';
                    widget.jobOrderDetailStore.fetchJobOrderDetailByLastModifiedDateTime(lastEditDateTimeStr);

                    widget.jobOrderStore.fetchJobOrderDetails();
                    break;
                  case 'update':
                    widget.jobOrderDetailStore.updateJobOrderDetail(widget.jobOrderDetailStore.jobOrderDetail);
                    _scaffoldJODKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Record Updated'),
                      ),
                    );
                    widget.jobOrderStore.fetchJobOrderDetails();
                    break;
                  default:
                    _scaffoldJODKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Default'),
                      ),
                    );
                    break;
                }
              }
              else {
                _scaffoldJODKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text('Error(s) Found'),
                  ),
                );
              }
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            /*initialValue: {
              'jo_detail_start': DateTime.now(),
              'jo_detail_end': DateTime.now(),
            },*/
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black54,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    child: Column(
                      children: <Widget>[
                        FormBuilderTextField(
                          attribute: 'jo_id',
                          decoration: InputDecoration(labelText: 'Job Order ID'),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.joID.toString(),
                        ),
                        FormBuilderTextField(
                          attribute: 'jo_detail_service_type',
                          decoration: InputDecoration(labelText: 'Service Type'),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.serviceType,
                          onChanged: (val) {
                            widget.jobOrderDetailStore.jobOrderDetail.serviceType = val;
                          },
                        ),
                        FormBuilderTextField(
                          attribute: 'jo_detail_client_name',
                          decoration: InputDecoration(labelText: 'Client'),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.clientName,
                          readOnly: true,
                        ),
                        FormBuilderTextField(
                          attribute: 'jo_detail_client_rep',
                          decoration: InputDecoration(labelText: 'Representative'),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.custRep,
                          onChanged: (val) {
                            widget.jobOrderDetailStore.jobOrderDetail.custRep = val;
                          },
                        ),
                        FormBuilderTextField(
                          attribute: 'jo_detail_descr',
                          decoration: InputDecoration(labelText: 'Work Description'),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.workDescr,
                          onChanged: (val) {
                            widget.jobOrderDetailStore.jobOrderDetail.workDescr = val;
                          },
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'jo_detail_start',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'Start'),
                          initialValue: isNull(widget.jobOrderDetailStore.jobOrderDetail.jodstart) ? DateTime.now() : DateFormat.jm().parse(widget.jobOrderDetailStore.jobOrderDetail.jodstart),
                          onChanged: (val) {
                            if (val == null) widget.jobOrderDetailStore.jobOrderDetail.jodstart = null;
                            else widget.jobOrderDetailStore.jobOrderDetail.jodstart = DateFormat.jm().format(val);
                            //validate
                          },
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'jo_detail_end',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'End'),
                          initialValue: isNull(widget.jobOrderDetailStore.jobOrderDetail.jodend) ? DateTime.now() : DateFormat.jm().parse(widget.jobOrderDetailStore.jobOrderDetail.jodend),
                          onChanged: (val) {
                            if (val == null) widget.jobOrderDetailStore.jobOrderDetail.jodend = null;
                            else widget.jobOrderDetailStore.jobOrderDetail.jodend = DateFormat.jm().format(val);
                            //validate
                          },
                        ),
                        FormBuilderSwitch(
                          attribute: 'jo_detail_charge',
                          label: Text(
                            'Charge',
                            textAlign: TextAlign.center,
                          ),
                          initialValue: widget.jobOrderDetailStore.jobOrderDetail.charge == 1 ? true : false,
                          onChanged: (val) {
                            if (val) widget.jobOrderDetailStore.jobOrderDetail.charge = 1;
                            else widget.jobOrderDetailStore.jobOrderDetail.charge = 0;
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          // Center(
          //   child: RaisedButton(
          //     onPressed: () {
          //       return null;
          //     },
          //     child: Text('Press Me'),
          //   ),
          // ),
        ],
      ),
    ),
    );
  }

}