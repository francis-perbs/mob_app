import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/job_order/job_order_expansion.dart';
import 'package:mob_app/store/job_order_detail_store.dart';
import 'package:mob_app/store/job_order_store.dart';
import 'package:provider/provider.dart';

class JobOrder extends StatefulWidget {
  final String appTitle;
  final JobOrderStore jobOrderStore;

  JobOrder({Key key, this.appTitle, this.jobOrderStore}) : super(key: key);

  @override
  _JobOrderState createState() => _JobOrderState(appTitle, jobOrderStore);
}

class _JobOrderState extends State<JobOrder> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  _JobOrderState(this.appTitle, this.jobOrderStore);

  String appTitle;
  JobOrderStore jobOrderStore;

  @override
  void initState() {
    super.initState();
    jobOrderStore.setupValidations();
  }

  @override
  void dispose() {
    jobOrderStore.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //final jobOrderStore = Provider.of<JobOrderStore>(context);
    
    return Observer(
      builder: (_) =>
      Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: 'Save Record',
            onPressed: () {
              //jobOrderStore.validateCustomerCode(jobOrderStore.jobOrder.custCode);
              //jobOrderStore.validateJobOrderDate(jobOrderStore.jobOrder.jobOrderDate);
              jobOrderStore.validateAllForJobOrder();
              if (!jobOrderStore.hasJobOrderValidationError) {
                switch (jobOrderStore.jobOrderSaveAction) {
                  case 'insert':
                    DateTime lastEditDateTime = DateTime.now();
                    String lastEditDateTimeStr = DateFormat.yMd().add_jm().format(lastEditDateTime);
                    jobOrderStore.setJobOrderLastModifiedDateTime(lastEditDateTimeStr);

                    jobOrderStore.insertJobOrder();

                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Record Saved'),
                      ),
                    );

                    jobOrderStore.jobOrderSaveAction = 'update';
                    jobOrderStore.fetchJobOrderByLastModifiedDateTime(lastEditDateTimeStr);

                    jobOrderStore.fetchJobOrders();
                    break;
                  case 'update':
                    jobOrderStore.updateJobOrder(jobOrderStore.jobOrder);
                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Record Updated'),
                      ),
                    );
                    jobOrderStore.fetchJobOrders();
                    break;
                  default:
                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Default'),
                      ),
                    );
                    break;
                }
              }
              else {
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text('Error(s) Found'),
                  ),
                );
              }
            },
          ),
        ],
      ),
      body: ListView(
            children: <Widget>[
              FormBuilder(
                //key: _jobOrderKey,
                /*initialValue: {
                  'job_order_date': DateTime.now(),
                },*/
                child: Container(
                  margin: EdgeInsets.all(5.0),
                  child: Column(
                    children: <Widget>[
                      ExpansionPanelReference(jobOrderStore),
                      ExpansionPanelFiles(jobOrderStore),
                      ExpansionPanelDetails(jobOrderStore),
                      ExpansionPanelRemarks(jobOrderStore),
                    ],
                  ),
                ),
              ),
              // Center(
              //   child: RaisedButton(
              //     onPressed: () {
              //       //jobOrderStore.validateCustomerCode(jobOrderStore.jobOrder.custCode);
              //     },
              //     child: Text('Press Me'),
              //   ),
              // ),
            ],
      ),
    ),
    );
  }
}