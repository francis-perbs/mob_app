import 'package:mobx/mobx.dart';

part 'claim_store.g.dart';

class ExpenseClaimStore = _ExpenseClaimStore with _$ExpenseClaimStore;

abstract class _ExpenseClaimStore with Store {
  @observable
  bool expandDescrPanel = true;

  @observable
  bool expandInfoPanel = false;

  @observable
  bool expandFilesPanel = false;

  @observable
  bool expandRemarksPanel = false;
}