import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

class ExpenseClaimDetail extends StatefulWidget {
  final String appTitle;

  ExpenseClaimDetail({Key key, this.appTitle}) : super(key: key);

  @override
  _ExpenseClaimDetailState createState() => _ExpenseClaimDetailState(this.appTitle);
}

class _ExpenseClaimDetailState extends State<ExpenseClaimDetail> {
  String appTitle;
  _ExpenseClaimDetailState(this.appTitle);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: 'Save Record',
            onPressed: () {
              return null;
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black54,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    child: Column(
                      children: <Widget>[
                        FormBuilderTextField(
                          attribute: 'ec_detail_jo_nbr',
                          decoration: InputDecoration(labelText: 'Job Order Nbr'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ec_detail_jo_date',
                          decoration: InputDecoration(labelText: 'Job Order Date'),
                          readOnly: true,
                        ),
                        FormBuilderTextField(
                          attribute: 'ec_detail_client',
                          decoration: InputDecoration(labelText: 'Client'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ec_detail_expense_type',
                          decoration: InputDecoration(labelText: 'Expense Type'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ec_detail_descr',
                          decoration: InputDecoration(labelText: 'Description'),
                        ),
                        FormBuilderTextField(
                          attribute: 'ec_detail_amt',
                          decoration: InputDecoration(labelText: 'Amount'),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                return null;
              },
              child: Text('Press Me'),
            ),
          ),
        ],
      ),
    );
  }
}