import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/claim/claim_store.dart';
import 'package:provider/provider.dart';

class ExpansionPanelExpenseClaimDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final expenseClaimStore = Provider.of<ExpenseClaimStore>(context);
    
    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        expenseClaimStore.expandDescrPanel = !expenseClaimStore.expandDescrPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: expenseClaimStore.expandDescrPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Description'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderDropdown(
                  attribute: 'claim_requestor',
                  decoration: InputDecoration(
                    labelText: "Requestor",
                    //errorText:
                  ),
                  initialValue: '',
                  items: ['','EMP001', 'EMP002'].map(
                    (item) => DropdownMenuItem(
                      value: item,
                      child: Text('$item'))
                  ).toList(),
                  onChanged: (val) {
                    //jobOrderStore.customerCode = val;
                    //jobOrderStore.validateCustomerCode(val);
                    return null;
                  },
                ),
                FormBuilderTextField(
                  attribute: 'claim_payee',
                  decoration: InputDecoration(labelText: 'Payee'),
                  readOnly: true,
                ),
                FormBuilderTextField(
                  attribute: 'claim_status',
                  decoration: InputDecoration(labelText: 'Doc Status'),
                  initialValue: 'Saved',
                  readOnly: true,
                ),
                FormBuilderTextField(
                  attribute: 'claim_num',
                  decoration: InputDecoration(labelText: 'Claim No.'),
                  readOnly: true,
                ),
                FormBuilderDateTimePicker(
                  attribute: 'claim_doc_date',
                  inputType: InputType.date,
                  format: DateFormat('dd/MM/yyyy'),
                  decoration: InputDecoration(labelText: 'Claim Doc Date'),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelExpenseClaimInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final expenseClaimStore = Provider.of<ExpenseClaimStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        expenseClaimStore.expandInfoPanel = !expenseClaimStore.expandInfoPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: expenseClaimStore.expandInfoPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Information'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            height: 300.0,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columns: [
                        DataColumn(label: Text('JO No.')),
                        DataColumn(label: Text('JO Date')),
                        DataColumn(label: Text('Client')),
                        DataColumn(label: Text('Expense Type')),
                        DataColumn(label: Text('Description')),
                        DataColumn(label: Text('Amount')),
                      ],
                      rows: [
                        DataRow(
                          cells: [
                            DataCell(Text('JO001')),
                            DataCell(Text('01/05/2020')),
                            DataCell(Text('Customer One')),
                            DataCell(Text('Travel Fee')),
                            DataCell(Text('Travel to client')),
                            DataCell(Text('200.00')),
                          ]
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelExpenseClaimFiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final expenseClaimStore = Provider.of<ExpenseClaimStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        expenseClaimStore.expandFilesPanel = !expenseClaimStore.expandFilesPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: expenseClaimStore.expandFilesPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Files'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                  ListTile(
                    title: Text('Image 1'),
                    subtitle: Text('Url: http://www.google.com'),
                  ),
                  ListTile(
                    title: Text('Image 2'),
                    subtitle: Text('Url: http://www.google.com'),
                  ),
                  ListTile(
                    title: Text('Image 3'),
                    subtitle: Text('Url: http://www.google.com'),
                  ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelExpenseClaimRemarks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final expenseClaimStore = Provider.of<ExpenseClaimStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        expenseClaimStore.expandRemarksPanel = !expenseClaimStore.expandRemarksPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: expenseClaimStore.expandRemarksPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Remarks'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderTextField(
                  attribute: 'claim_remarks',
                  decoration: InputDecoration(labelText: 'Remarks'),
                  minLines: 3,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}