import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mob_app/home/claim/claim_expansion.dart';
import 'package:mob_app/home/claim/claim_store.dart';
import 'package:provider/provider.dart';

class ExpenseClaim extends StatefulWidget {
  final String appTitle;

  ExpenseClaim({Key key, this.appTitle}) : super(key: key);

  @override
  _ExpenseClaimState createState() => _ExpenseClaimState(this.appTitle);
}

class _ExpenseClaimState extends State<ExpenseClaim> {
  String appTitle;
  _ExpenseClaimState(this.appTitle);

  final ExpenseClaimStore expenseClaimStore = ExpenseClaimStore();

  @override
  Widget build(BuildContext context) => Provider<ExpenseClaimStore> (
    create: (_) => expenseClaimStore,
    child: Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: null
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            initialValue: {
              'claim_doc_date': DateTime.now(),
            },
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  ExpansionPanelExpenseClaimDescription(),
                  ExpansionPanelExpenseClaimInformation(),
                  ExpansionPanelExpenseClaimFiles(),
                  ExpansionPanelExpenseClaimRemarks(),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}