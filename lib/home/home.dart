import 'package:flutter/material.dart';
import 'package:mob_app/about/about.dart';
import 'package:mob_app/home/claim/claim.dart';
import 'package:mob_app/home/home_tab.dart';
import 'package:mob_app/home/job_order/job_order.dart';
import 'package:mob_app/home/leave_app/leave_app.dart';
import 'package:mob_app/home/overtime/overtime.dart';
import 'package:mob_app/login/login.dart';
import 'package:mob_app/logout/logout.dart';

class HomeApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Attendance App',
      //home: HomeTab(),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeTab(),
        '/about': (context) => About(),
        '/login': (context) => Login(),
        '/logout': (context) => Logout(),
        '/joborder': (context) => JobOrder(),
        '/leaveapp': (context) => LeaveApp(),
        '/overtime': (context) => Overtime(),
        '/claim': (context) => ExpenseClaim(),
      },
    );
  }
  
}