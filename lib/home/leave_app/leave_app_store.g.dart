// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leave_app_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LeaveAppStore on _LeaveAppStore, Store {
  final _$expandDescrPanelAtom = Atom(name: '_LeaveAppStore.expandDescrPanel');

  @override
  bool get expandDescrPanel {
    _$expandDescrPanelAtom.context.enforceReadPolicy(_$expandDescrPanelAtom);
    _$expandDescrPanelAtom.reportObserved();
    return super.expandDescrPanel;
  }

  @override
  set expandDescrPanel(bool value) {
    _$expandDescrPanelAtom.context.conditionallyRunInAction(() {
      super.expandDescrPanel = value;
      _$expandDescrPanelAtom.reportChanged();
    }, _$expandDescrPanelAtom, name: '${_$expandDescrPanelAtom.name}_set');
  }

  final _$expandInfoPanelAtom = Atom(name: '_LeaveAppStore.expandInfoPanel');

  @override
  bool get expandInfoPanel {
    _$expandInfoPanelAtom.context.enforceReadPolicy(_$expandInfoPanelAtom);
    _$expandInfoPanelAtom.reportObserved();
    return super.expandInfoPanel;
  }

  @override
  set expandInfoPanel(bool value) {
    _$expandInfoPanelAtom.context.conditionallyRunInAction(() {
      super.expandInfoPanel = value;
      _$expandInfoPanelAtom.reportChanged();
    }, _$expandInfoPanelAtom, name: '${_$expandInfoPanelAtom.name}_set');
  }

  final _$expandRemarksPanelAtom =
      Atom(name: '_LeaveAppStore.expandRemarksPanel');

  @override
  bool get expandRemarksPanel {
    _$expandRemarksPanelAtom.context
        .enforceReadPolicy(_$expandRemarksPanelAtom);
    _$expandRemarksPanelAtom.reportObserved();
    return super.expandRemarksPanel;
  }

  @override
  set expandRemarksPanel(bool value) {
    _$expandRemarksPanelAtom.context.conditionallyRunInAction(() {
      super.expandRemarksPanel = value;
      _$expandRemarksPanelAtom.reportChanged();
    }, _$expandRemarksPanelAtom, name: '${_$expandRemarksPanelAtom.name}_set');
  }

  @override
  String toString() {
    final string =
        'expandDescrPanel: ${expandDescrPanel.toString()},expandInfoPanel: ${expandInfoPanel.toString()},expandRemarksPanel: ${expandRemarksPanel.toString()}';
    return '{$string}';
  }
}
