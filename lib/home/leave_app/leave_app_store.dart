import 'package:mobx/mobx.dart';

part 'leave_app_store.g.dart';

class LeaveAppStore = _LeaveAppStore with _$LeaveAppStore;

abstract class _LeaveAppStore with Store {
  @observable
  bool expandDescrPanel = true;

  @observable
  bool expandInfoPanel = false;

  @observable
  bool expandRemarksPanel = false;
}