import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:mob_app/home/leave_app/leave_app_store.dart';
import 'package:provider/provider.dart';

class ExpansionPanelLeaveDescription extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final leaveAppStore = Provider.of<LeaveAppStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        leaveAppStore.expandDescrPanel = !leaveAppStore.expandDescrPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: leaveAppStore.expandDescrPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Description'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderDropdown(
                  attribute: 'emp_code',
                  decoration: InputDecoration(
                    labelText: "Employee Code",
                    //errorText:
                  ),
                  initialValue: '',
                  items: ['','EMP001', 'EMP002'].map(
                    (item) => DropdownMenuItem(
                      value: item,
                      child: Text('$item'))
                  ).toList(),
                  onChanged: (val) {
                    //jobOrderStore.customerCode = val;
                    //jobOrderStore.validateCustomerCode(val);
                    return null;
                  },
                ),
                FormBuilderTextField(
                  attribute: 'emp_name',
                  decoration: InputDecoration(labelText: 'Employee Name'),
                  readOnly: true,
                ),
                FormBuilderTextField(
                  attribute: 'emp_dept',
                  decoration: InputDecoration(labelText: 'Department'),
                ),
                FormBuilderTextField(
                  attribute: 'emp_req_num',
                  decoration: InputDecoration(labelText: 'Req. Ref. Nbr.'),
                  readOnly: true,
                ),
                FormBuilderDateTimePicker(
                  attribute: 'emp_req_date',
                  inputType: InputType.date,
                  format: DateFormat('dd/MM/yyyy'),
                  decoration: InputDecoration(labelText: 'Req. Date'),
                ),
                FormBuilderTextField(
                  attribute: 'emp_req_status',
                  decoration: InputDecoration(labelText: 'Req. Status'),
                  initialValue: 'Saved',
                  readOnly: true,
                ),
                FormBuilderDropdown(
                  attribute: 'emp_nature_leave',
                  decoration: InputDecoration(
                    labelText: "Leave Type",
                    //errorText:
                  ),
                  initialValue: '',
                  items: ['','Vacation', 'Sick'].map(
                    (item) => DropdownMenuItem(
                      value: item,
                      child: Text('$item'))
                  ).toList(),
                  onChanged: (val) {
                    //jobOrderStore.customerCode = val;
                    //jobOrderStore.validateCustomerCode(val);
                    return null;
                  },
                ),
                FormBuilderTouchSpin(
                  attribute: 'emp_leave_days',
                  decoration: InputDecoration(labelText: 'No. of Days'),
                  initialValue: 1,
                  step: 1,
                ),
                FormBuilderSwitch(
                  attribute: 'emp_leave_paid',
                  label: Text(
                    'Paid Leave?',
                    textAlign: TextAlign.center,
                  ),
                  initialValue: false,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelLeaveInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final leaveAppStore = Provider.of<LeaveAppStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        leaveAppStore.expandInfoPanel = !leaveAppStore.expandInfoPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: leaveAppStore.expandInfoPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Information'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            height: 300.0,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columns: [
                        DataColumn(label: Text('Date')),
                        DataColumn(label: Text('Description')),
                        DataColumn(label: Text('From')),
                        DataColumn(label: Text('To')),
                        DataColumn(label: Text('Total Hrs')),
                      ],
                      rows: [
                        DataRow(
                          cells: [
                            DataCell(Text('01/05/2020')),
                            DataCell(Text('Vacation Leave')),
                            DataCell(Text('8:30 AM')),
                            DataCell(Text('6:00 PM')),
                            DataCell(Text('8')),
                          ]
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
    );
  }
}

class ExpansionPanelLeaveRemarks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final leaveAppStore = Provider.of<LeaveAppStore>(context);

    return Observer(
      builder: (_) =>
      ExpansionPanelList(
      expansionCallback: (int item, bool status) {
        leaveAppStore.expandRemarksPanel = !leaveAppStore.expandRemarksPanel;
      },
      children: [
        ExpansionPanel(
          isExpanded: leaveAppStore.expandRemarksPanel,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Text('Remarks'),
            );
          },
          body: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black54,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Column(
              children: <Widget>[
                FormBuilderTextField(
                  attribute: 'emp_remarks',
                  decoration: InputDecoration(labelText: 'Remarks'),
                  minLines: 3,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
    );
  }
}