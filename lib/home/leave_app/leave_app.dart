import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mob_app/home/leave_app/leave_app_expansion.dart';
import 'package:mob_app/home/leave_app/leave_app_store.dart';
import 'package:provider/provider.dart';

class LeaveApp extends StatefulWidget {
  final String appTitle;

  LeaveApp({Key key, this.appTitle}) : super(key: key);

  @override
  _LeaveAppState createState() => _LeaveAppState(this.appTitle);
}

class _LeaveAppState extends State<LeaveApp> {
  String appTitle;
  _LeaveAppState(this.appTitle);

  final LeaveAppStore leaveAppStore = LeaveAppStore();

  @override
  Widget build(BuildContext context) => Provider<LeaveAppStore> (
    create: (_) => leaveAppStore,
    child: Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: null
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            initialValue: {
              'emp_req_date': DateTime.now(),
            },
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  ExpansionPanelLeaveDescription(),
                  ExpansionPanelLeaveInformation(),
                  ExpansionPanelLeaveRemarks(),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}