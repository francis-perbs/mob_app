import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

class LeaveAppDetail extends StatefulWidget {
  final String appTitle;

  LeaveAppDetail({Key key, this.appTitle}) : super(key: key);

  @override
  _LeaveAppDetailState createState() => _LeaveAppDetailState(this.appTitle);
}

class _LeaveAppDetailState extends State<LeaveAppDetail> {
  String appTitle;
  _LeaveAppDetailState(this.appTitle);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$appTitle'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: 'Save Record',
            onPressed: () {
              return null;
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FormBuilder(
            initialValue: {
              'leave_detail_date': DateTime.now(),
              'leave_detail_from': DateTime.now(),
              'leave_detail_to': DateTime.now(),
            },
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black54,
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    child: Column(
                      children: <Widget>[
                        FormBuilderDateTimePicker(
                          attribute: 'leave_detail_date',
                          inputType: InputType.date,
                          format: DateFormat.yMd(),
                          decoration: InputDecoration(labelText: 'Date'),
                        ),
                        FormBuilderTextField(
                          attribute: 'leave_detail_descr',
                          decoration: InputDecoration(labelText: 'Leave Description'),
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'leave_detail_from',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'From'),
                        ),
                        FormBuilderDateTimePicker(
                          attribute: 'leave_detail_to',
                          inputType: InputType.time,
                          format: DateFormat.jm(),
                          decoration: InputDecoration(labelText: 'To'),
                        ),
                        FormBuilderTextField(
                          attribute: 'leave_detail_total_hrs',
                          decoration: InputDecoration(labelText: 'No. of Hrs'),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                return null;
              },
              child: Text('Press Me'),
            ),
          ),
        ],
      ),
    );
  }
}