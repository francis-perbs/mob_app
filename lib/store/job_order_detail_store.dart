import 'package:mob_app/models/job_order.dart';
import 'package:mob_app/models/job_order_detail.dart';
import 'package:mob_app/provider/db.dart';
import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';

part 'job_order_detail_store.g.dart';

class JobOrderDetailStore = _JobOrderDetailStore with _$JobOrderDetailStore;

abstract class _JobOrderDetailStore with Store {
  @observable
  JobOrderDetailErrorState jobOrderDetailError = JobOrderDetailErrorState();

  // @observable
  // List<JobOrderDetailObj> jobOrderDetails = [];

  // @observable
  // ObservableList<JobOrderDetailObj> selectedJobOrderDetails = ObservableList<JobOrderDetailObj>();

  @observable
  JobOrderDetailObj jobOrderDetail = JobOrderDetailObj();

  @observable
  String jobOrderDetailSaveAction;

  List<ReactionDisposer> _disposers;

  void setupValidations() {
    _disposers = [
      // reaction((_) => jobOrder.custCode, validateCustomerCode),
      // reaction((_) => jobOrder.jobOrderDate, validateJobOrderDate),
      // reaction((_) => jobOrder.jobOrderNbr, validateJobOrderNbr),
      // reaction((_) => jobOrder.requesterID, validateRequesterID),
    ];
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }

  @action
  void validateAllForJobOrderDetail() {
    // validateCustomerCode(jobOrder.custCode);
    // validateJobOrderNbr(jobOrder.jobOrderNbr);
    // validateJobOrderDate(jobOrder.jobOrderDate);
    // validateRequesterID(jobOrder.requesterID);
  }

  @computed
  bool get hasJobOrderDetailValidationError => false;

  @action
  void resetJobOrderDetail() {
    jobOrderDetail = JobOrderDetailObj();
    jobOrderDetailError = JobOrderDetailErrorState();
  }

  // @action
  // void resetSelectedJobOrderDetails() {
  //   selectedJobOrderDetails = ObservableList<JobOrderDetailObj>();
  // }

  @action
  void setJobOrderDetailLastModifiedDateTime(String str) {
    jobOrderDetail.lastModifiedDateTime = str;
  }

  // @action
  // Future<void> fetchJobOrderDetails() async {
  //   jobOrderDetails = [];
  //     final future = await DB.queryJobOrderDetailsByParentID(JobOrderDetailObj.table, jobOrderDetail.id);
  //     jobOrderDetails = future.map(
  //       (item) => JobOrderDetailObj.fromMap(item)
  //       ).toList();
  // }

  @action
  Future<void> fetchJobOrderDetailByID(int jobOrderID) async {
    final future = await DB.queryOneByID(JobOrderDetailObj.table, jobOrderID);
    final jobOrderDetails = future.map(
      (item) => JobOrderDetailObj.fromMap(item)
      ).toList();
    jobOrderDetail = jobOrderDetails.first;
  }

  @action
  Future<void> fetchJobOrderDetailByLastModifiedDateTime(String modifiedDateTime) async {
    final future = await DB.queryOneByLastModifiedDateTime(JobOrderDetailObj.table, modifiedDateTime);
    final jobOrderDetails = future.map(
      (item) => JobOrderDetailObj.fromMap(item)
      ).toList();
    jobOrderDetail = jobOrderDetails.first;
  }

  @action
  Future<void> insertJobOrderDetail() async {
    await DB.insertJobOrderDetail(JobOrderDetailObj.table, jobOrderDetail);
  }

  // @action
  // Future<void> deleteJobOrderDetail(JobOrderDetailObj jobOrderDetail) async {
  //   await DB.deleteJobOrderDetail(JobOrderDetailObj.table, jobOrderDetail);
  // }

  @action
  Future<void> updateJobOrderDetail(JobOrderDetailObj jobOrderDetail) async {
    await DB.updateJobOrderDetail(JobOrderDetailObj.table, jobOrderDetail);
  }
}

class JobOrderDetailErrorState = _JobOrderDetailErrorState with _$JobOrderDetailErrorState;
abstract class _JobOrderDetailErrorState with Store {

}