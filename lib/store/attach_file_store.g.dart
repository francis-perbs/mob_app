// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attach_file_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AttachFileStore on _AttachFileStore, Store {
  Computed<String> _$filePathComputed;

  @override
  String get filePath =>
      (_$filePathComputed ??= Computed<String>(() => super.filePath)).value;

  final _$jobOrderFileAtom = Atom(name: '_AttachFileStore.jobOrderFile');

  @override
  JobOrderFileObj get jobOrderFile {
    _$jobOrderFileAtom.context.enforceReadPolicy(_$jobOrderFileAtom);
    _$jobOrderFileAtom.reportObserved();
    return super.jobOrderFile;
  }

  @override
  set jobOrderFile(JobOrderFileObj value) {
    _$jobOrderFileAtom.context.conditionallyRunInAction(() {
      super.jobOrderFile = value;
      _$jobOrderFileAtom.reportChanged();
    }, _$jobOrderFileAtom, name: '${_$jobOrderFileAtom.name}_set');
  }

  final _$imageAtom = Atom(name: '_AttachFileStore.image');

  @override
  File get image {
    _$imageAtom.context.enforceReadPolicy(_$imageAtom);
    _$imageAtom.reportObserved();
    return super.image;
  }

  @override
  set image(File value) {
    _$imageAtom.context.conditionallyRunInAction(() {
      super.image = value;
      _$imageAtom.reportChanged();
    }, _$imageAtom, name: '${_$imageAtom.name}_set');
  }

  final _$insertJobOrderFileAsyncAction = AsyncAction('insertJobOrderFile');

  @override
  Future<void> insertJobOrderFile() {
    return _$insertJobOrderFileAsyncAction
        .run(() => super.insertJobOrderFile());
  }

  final _$deleteJobOrderFileAsyncAction = AsyncAction('deleteJobOrderFile');

  @override
  Future<void> deleteJobOrderFile(JobOrderFileObj jobOrderFile) {
    return _$deleteJobOrderFileAsyncAction
        .run(() => super.deleteJobOrderFile(jobOrderFile));
  }

  final _$_AttachFileStoreActionController =
      ActionController(name: '_AttachFileStore');

  @override
  void resetJobOrderFile() {
    final _$actionInfo = _$_AttachFileStoreActionController.startAction();
    try {
      return super.resetJobOrderFile();
    } finally {
      _$_AttachFileStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setJobOrderFileLastModifiedDateTime(String str) {
    final _$actionInfo = _$_AttachFileStoreActionController.startAction();
    try {
      return super.setJobOrderFileLastModifiedDateTime(str);
    } finally {
      _$_AttachFileStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<File> saveJobOrderFileToApp() {
    final _$actionInfo = _$_AttachFileStoreActionController.startAction();
    try {
      return super.saveJobOrderFileToApp();
    } finally {
      _$_AttachFileStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'jobOrderFile: ${jobOrderFile.toString()},image: ${image.toString()},filePath: ${filePath.toString()}';
    return '{$string}';
  }
}
