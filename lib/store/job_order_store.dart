import 'package:mob_app/models/job_order.dart';
import 'package:mob_app/models/job_order_detail.dart';
import 'package:mob_app/models/job_order_file.dart';
import 'package:mob_app/provider/db.dart';
import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';

part 'job_order_store.g.dart';

class JobOrderStore = _JobOrderStore with _$JobOrderStore;

abstract class _JobOrderStore with Store {
  @observable
  JobOrderErrorState jobOrderError = JobOrderErrorState();

  @observable
  List<JobOrderObj> jobOrders = [];

  @observable
  ObservableList<JobOrderObj> jobOrdersObservable = ObservableList<JobOrderObj>();

  @observable
  JobOrderObj jobOrder = JobOrderObj();

  @observable
  bool expandRefPanel = true;

  @observable
  bool expandFilePanel = false;

  @observable
  bool expandDetailsPanel = false;

  @observable
  bool expandRemarksPanel = false;

  // @observable
  // bool detailsPanelSelected = false;

  @observable
  String jobOrderSaveAction;

  List<ReactionDisposer> _disposers;

  void setupValidations() {
    _disposers = [
      reaction((_) => jobOrder.custCode, validateCustomerCode),
      reaction((_) => jobOrder.jobOrderDate, validateJobOrderDate),
      reaction((_) => jobOrder.jobOrderNbr, validateJobOrderNbr),
      reaction((_) => jobOrder.requesterID, validateRequesterID),
    ];
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }

  @action
  void validateCustomerCode(String str) {
    if (isNull(str) || str.isEmpty) {
      jobOrderError.custCode = 'Customer Code is required';
      expandRefPanel = true;
      return;
    }
    jobOrderError.custCode = null;
  }

  @action
  void validateJobOrderNbr(String str) {
    if (isNull(str) || str.isEmpty) {
      jobOrderError.jobOrderNbr = 'Job Order Nbr is required';
      expandRefPanel = true;
      return;
    }
    jobOrderError.jobOrderNbr = null;
  }

  @action
  void validateJobOrderDate(String str) {
    if (isNull(str) || str.isEmpty) {
      jobOrderError.jobOrderDate = 'Job Order Date is required';
      expandRefPanel = true;
      return;
    }
    jobOrderError.jobOrderDate = null;
  }

  @action
  void validateRequesterID(String str) {
    if (isNull(str) || str.isEmpty) {
      jobOrderError.requesterID = 'Requester ID is required';
      expandRefPanel = true;
      return;
    }
    jobOrderError.requesterID = null;
  }

  @action
  void validateAllForJobOrder() {
    validateCustomerCode(jobOrder.custCode);
    validateJobOrderNbr(jobOrder.jobOrderNbr);
    validateJobOrderDate(jobOrder.jobOrderDate);
    validateRequesterID(jobOrder.requesterID);
  }

  @computed
  bool get hasJobOrderValidationError => jobOrderError.custCode != null || jobOrderError.jobOrderNbr != null || jobOrderError.jobOrderDate != null || jobOrderError.requesterID != null;

  @action
  void resetJobOrder() {
    jobOrder = JobOrderObj();
    jobOrderError = JobOrderErrorState();
  }

  @action
  void setJobOrderLastModifiedDateTime(String str) {
    jobOrder.lastModifiedDateTime = str;
  }

  @action
  Future<void> fetchJobOrders() async {
    jobOrders = [];
    final future = await DB.query(JobOrderObj.table);
    jobOrders = future.map(
      (item) => JobOrderObj.fromMap(item)
      ).toList();
    jobOrdersObservable = ObservableList.of(jobOrders);
  }

  @action
  Future<void> fetchJobOrderByID(int jobOrderID) async {
    final future = await DB.queryOneByID(JobOrderObj.table, jobOrderID);
    jobOrders = future.map(
      (item) => JobOrderObj.fromMap(item)
      ).toList();
    jobOrder = jobOrders.first;
  }

  @action
  Future<void> fetchJobOrderByLastModifiedDateTime(String modifiedDateTime) async {
    final future = await DB.queryOneByLastModifiedDateTime(JobOrderObj.table, modifiedDateTime);
    jobOrders = future.map(
      (item) => JobOrderObj.fromMap(item)
      ).toList();
    jobOrder = jobOrders.first;
  }

  @action
  Future<void> insertJobOrder() async {
    await DB.insertJobOrder(JobOrderObj.table, jobOrder);
  }

  @action
  Future<void> deleteJobOrder(JobOrderObj jobOrder) async {
    await DB.deleteJobOrder(JobOrderObj.table, jobOrder);
  }

  @action
  Future<void> updateJobOrder(JobOrderObj jobOrder) async {
    await DB.updateJobOrder(JobOrderObj.table, jobOrder);
  }

  ///
  ///Job Order Detail
  ///
  
  List<JobOrderDetailObj> jobOrderDetails = [];

  @observable
  ObservableList<JobOrderDetailObj> jobOrderDetailsObservable = ObservableList<JobOrderDetailObj>();

  @observable
  ObservableList<JobOrderDetailObj> selectedJobOrderDetails = ObservableList<JobOrderDetailObj>();

  @action
  void resetSelectedJobOrderDetails() {
    selectedJobOrderDetails = ObservableList<JobOrderDetailObj>();
  }

  @action
  void resetJobOrderDetailObservable() {
    jobOrderDetailsObservable = ObservableList<JobOrderDetailObj>();
    //jobOrder.joDetails = [];
  }

  @action
  Future<void> fetchJobOrderDetails() async {
    jobOrderDetails = [];
      final future = await DB.queryJobOrderDetailsByParentID(JobOrderDetailObj.table, jobOrder.id);
      // jobOrder.joDetails = future.map(
      //   (item) => JobOrderDetailObj.fromMap(item)
      // ).toList();
      //jobOrderDetailsObservable = ObservableList.of(jobOrder.joDetails);

      jobOrderDetails = future.map(
        (item) => JobOrderDetailObj.fromMap(item)
      ).toList();
      jobOrderDetailsObservable = ObservableList.of(jobOrderDetails);
  }

  @action
  Future<void> deleteJobOrderDetail(JobOrderDetailObj jobOrderDetail) async {
    await DB.deleteJobOrderDetail(JobOrderDetailObj.table, jobOrderDetail);
  }

  ///
  ///Job Order File
  ///
  
  List<JobOrderFileObj> jobOrderFiles = [];

  @observable
  ObservableList<JobOrderFileObj> jobOrderFilesObservable = ObservableList<JobOrderFileObj>();

  @action
  void resetJobOrderFilesObservable() {
    jobOrderFilesObservable = ObservableList<JobOrderFileObj>();
  }

  @action
  Future<void> fetchJobOrderFiles() async {
    jobOrderFiles = [];
    final future = await DB.queryJobOrderFilesByParentID(JobOrderFileObj.table, jobOrder.id);
    jobOrderFiles = future.map(
      (item) => JobOrderFileObj.fromMap(item)
    ).toList();
    jobOrderFilesObservable = ObservableList.of(jobOrderFiles);
  }

  // @action
  // Future<void> deleteJobOrderFile(JobOrderFileObj jobOrderFile) async {
  //   await DB.deleteJobOrderFile(JobOrderFileObj.table, jobOrderFile);
  // }
  
}

class JobOrderErrorState = _JobOrderErrorState with _$JobOrderErrorState;
abstract class _JobOrderErrorState with Store {
  @observable
  String custCode;
  
  @observable
  String jobOrderNbr;

  @observable
  String jobOrderDate;

  @observable
  String requesterID;
}