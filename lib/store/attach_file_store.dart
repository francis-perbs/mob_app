import 'dart:io';

import 'package:mob_app/models/job_order_file.dart';
import 'package:mob_app/provider/db.dart';
import 'package:mobx/mobx.dart';

part 'attach_file_store.g.dart';

class AttachFileStore = _AttachFileStore with _$AttachFileStore;

abstract class _AttachFileStore with Store {

  @observable
  JobOrderFileObj jobOrderFile = JobOrderFileObj();

  @computed
  String get filePath => '${jobOrderFile.imgPath}${jobOrderFile.imgName}';

  @observable
  File image;

  @action
  void resetJobOrderFile() {
    jobOrderFile = JobOrderFileObj();
  }

  @action
  void setJobOrderFileLastModifiedDateTime(String str) {
    jobOrderFile.lastModifiedDateTime = str;
  }

  @action
  Future<File> saveJobOrderFileToApp() {
    String newPath = jobOrderFile.imgPath + '/job_order_files/';
    Directory(newPath).create();
    jobOrderFile.imgPath = newPath;
    return image.copy(jobOrderFile.imgPath + jobOrderFile.imgName);
  }

  @action
  Future<void> insertJobOrderFile() async {
    await DB.insertJobOrderFile(JobOrderFileObj.table, jobOrderFile);
  }

  @action
  Future<void> deleteJobOrderFile(JobOrderFileObj jobOrderFile) async {
    await DB.deleteJobOrderFile(JobOrderFileObj.table, jobOrderFile);
  }

  // @action
  // Future<void> fetchJobOrderFileByLastModifiedDateTime(String modifiedDateTime) async {
  //   final future = await DB.queryOneByLastModifiedDateTime(JobOrderFileObj.table, modifiedDateTime);
  //   final jobOrderFiles = future.map(
  //     (item) => JobOrderFileObj.fromMap(item)
  //     ).toList();
  //   jobOrderFile = jobOrderFiles.first;
  // }
}