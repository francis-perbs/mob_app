// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_order_detail_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$JobOrderDetailStore on _JobOrderDetailStore, Store {
  Computed<bool> _$hasJobOrderDetailValidationErrorComputed;

  @override
  bool get hasJobOrderDetailValidationError =>
      (_$hasJobOrderDetailValidationErrorComputed ??=
              Computed<bool>(() => super.hasJobOrderDetailValidationError))
          .value;

  final _$jobOrderDetailErrorAtom =
      Atom(name: '_JobOrderDetailStore.jobOrderDetailError');

  @override
  JobOrderDetailErrorState get jobOrderDetailError {
    _$jobOrderDetailErrorAtom.context
        .enforceReadPolicy(_$jobOrderDetailErrorAtom);
    _$jobOrderDetailErrorAtom.reportObserved();
    return super.jobOrderDetailError;
  }

  @override
  set jobOrderDetailError(JobOrderDetailErrorState value) {
    _$jobOrderDetailErrorAtom.context.conditionallyRunInAction(() {
      super.jobOrderDetailError = value;
      _$jobOrderDetailErrorAtom.reportChanged();
    }, _$jobOrderDetailErrorAtom,
        name: '${_$jobOrderDetailErrorAtom.name}_set');
  }

  final _$jobOrderDetailAtom =
      Atom(name: '_JobOrderDetailStore.jobOrderDetail');

  @override
  JobOrderDetailObj get jobOrderDetail {
    _$jobOrderDetailAtom.context.enforceReadPolicy(_$jobOrderDetailAtom);
    _$jobOrderDetailAtom.reportObserved();
    return super.jobOrderDetail;
  }

  @override
  set jobOrderDetail(JobOrderDetailObj value) {
    _$jobOrderDetailAtom.context.conditionallyRunInAction(() {
      super.jobOrderDetail = value;
      _$jobOrderDetailAtom.reportChanged();
    }, _$jobOrderDetailAtom, name: '${_$jobOrderDetailAtom.name}_set');
  }

  final _$jobOrderDetailSaveActionAtom =
      Atom(name: '_JobOrderDetailStore.jobOrderDetailSaveAction');

  @override
  String get jobOrderDetailSaveAction {
    _$jobOrderDetailSaveActionAtom.context
        .enforceReadPolicy(_$jobOrderDetailSaveActionAtom);
    _$jobOrderDetailSaveActionAtom.reportObserved();
    return super.jobOrderDetailSaveAction;
  }

  @override
  set jobOrderDetailSaveAction(String value) {
    _$jobOrderDetailSaveActionAtom.context.conditionallyRunInAction(() {
      super.jobOrderDetailSaveAction = value;
      _$jobOrderDetailSaveActionAtom.reportChanged();
    }, _$jobOrderDetailSaveActionAtom,
        name: '${_$jobOrderDetailSaveActionAtom.name}_set');
  }

  final _$fetchJobOrderDetailByIDAsyncAction =
      AsyncAction('fetchJobOrderDetailByID');

  @override
  Future<void> fetchJobOrderDetailByID(int jobOrderID) {
    return _$fetchJobOrderDetailByIDAsyncAction
        .run(() => super.fetchJobOrderDetailByID(jobOrderID));
  }

  final _$fetchJobOrderDetailByLastModifiedDateTimeAsyncAction =
      AsyncAction('fetchJobOrderDetailByLastModifiedDateTime');

  @override
  Future<void> fetchJobOrderDetailByLastModifiedDateTime(
      String modifiedDateTime) {
    return _$fetchJobOrderDetailByLastModifiedDateTimeAsyncAction.run(() =>
        super.fetchJobOrderDetailByLastModifiedDateTime(modifiedDateTime));
  }

  final _$insertJobOrderDetailAsyncAction = AsyncAction('insertJobOrderDetail');

  @override
  Future<void> insertJobOrderDetail() {
    return _$insertJobOrderDetailAsyncAction
        .run(() => super.insertJobOrderDetail());
  }

  final _$updateJobOrderDetailAsyncAction = AsyncAction('updateJobOrderDetail');

  @override
  Future<void> updateJobOrderDetail(JobOrderDetailObj jobOrderDetail) {
    return _$updateJobOrderDetailAsyncAction
        .run(() => super.updateJobOrderDetail(jobOrderDetail));
  }

  final _$_JobOrderDetailStoreActionController =
      ActionController(name: '_JobOrderDetailStore');

  @override
  void validateAllForJobOrderDetail() {
    final _$actionInfo = _$_JobOrderDetailStoreActionController.startAction();
    try {
      return super.validateAllForJobOrderDetail();
    } finally {
      _$_JobOrderDetailStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetJobOrderDetail() {
    final _$actionInfo = _$_JobOrderDetailStoreActionController.startAction();
    try {
      return super.resetJobOrderDetail();
    } finally {
      _$_JobOrderDetailStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setJobOrderDetailLastModifiedDateTime(String str) {
    final _$actionInfo = _$_JobOrderDetailStoreActionController.startAction();
    try {
      return super.setJobOrderDetailLastModifiedDateTime(str);
    } finally {
      _$_JobOrderDetailStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'jobOrderDetailError: ${jobOrderDetailError.toString()},jobOrderDetail: ${jobOrderDetail.toString()},jobOrderDetailSaveAction: ${jobOrderDetailSaveAction.toString()},hasJobOrderDetailValidationError: ${hasJobOrderDetailValidationError.toString()}';
    return '{$string}';
  }
}

mixin _$JobOrderDetailErrorState on _JobOrderDetailErrorState, Store {
  @override
  String toString() {
    final string = '';
    return '{$string}';
  }
}
