// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_order_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$JobOrderStore on _JobOrderStore, Store {
  Computed<bool> _$hasJobOrderValidationErrorComputed;

  @override
  bool get hasJobOrderValidationError =>
      (_$hasJobOrderValidationErrorComputed ??=
              Computed<bool>(() => super.hasJobOrderValidationError))
          .value;

  final _$jobOrderErrorAtom = Atom(name: '_JobOrderStore.jobOrderError');

  @override
  JobOrderErrorState get jobOrderError {
    _$jobOrderErrorAtom.context.enforceReadPolicy(_$jobOrderErrorAtom);
    _$jobOrderErrorAtom.reportObserved();
    return super.jobOrderError;
  }

  @override
  set jobOrderError(JobOrderErrorState value) {
    _$jobOrderErrorAtom.context.conditionallyRunInAction(() {
      super.jobOrderError = value;
      _$jobOrderErrorAtom.reportChanged();
    }, _$jobOrderErrorAtom, name: '${_$jobOrderErrorAtom.name}_set');
  }

  final _$jobOrdersAtom = Atom(name: '_JobOrderStore.jobOrders');

  @override
  List<JobOrderObj> get jobOrders {
    _$jobOrdersAtom.context.enforceReadPolicy(_$jobOrdersAtom);
    _$jobOrdersAtom.reportObserved();
    return super.jobOrders;
  }

  @override
  set jobOrders(List<JobOrderObj> value) {
    _$jobOrdersAtom.context.conditionallyRunInAction(() {
      super.jobOrders = value;
      _$jobOrdersAtom.reportChanged();
    }, _$jobOrdersAtom, name: '${_$jobOrdersAtom.name}_set');
  }

  final _$jobOrdersObservableAtom =
      Atom(name: '_JobOrderStore.jobOrdersObservable');

  @override
  ObservableList<JobOrderObj> get jobOrdersObservable {
    _$jobOrdersObservableAtom.context
        .enforceReadPolicy(_$jobOrdersObservableAtom);
    _$jobOrdersObservableAtom.reportObserved();
    return super.jobOrdersObservable;
  }

  @override
  set jobOrdersObservable(ObservableList<JobOrderObj> value) {
    _$jobOrdersObservableAtom.context.conditionallyRunInAction(() {
      super.jobOrdersObservable = value;
      _$jobOrdersObservableAtom.reportChanged();
    }, _$jobOrdersObservableAtom,
        name: '${_$jobOrdersObservableAtom.name}_set');
  }

  final _$jobOrderAtom = Atom(name: '_JobOrderStore.jobOrder');

  @override
  JobOrderObj get jobOrder {
    _$jobOrderAtom.context.enforceReadPolicy(_$jobOrderAtom);
    _$jobOrderAtom.reportObserved();
    return super.jobOrder;
  }

  @override
  set jobOrder(JobOrderObj value) {
    _$jobOrderAtom.context.conditionallyRunInAction(() {
      super.jobOrder = value;
      _$jobOrderAtom.reportChanged();
    }, _$jobOrderAtom, name: '${_$jobOrderAtom.name}_set');
  }

  final _$expandRefPanelAtom = Atom(name: '_JobOrderStore.expandRefPanel');

  @override
  bool get expandRefPanel {
    _$expandRefPanelAtom.context.enforceReadPolicy(_$expandRefPanelAtom);
    _$expandRefPanelAtom.reportObserved();
    return super.expandRefPanel;
  }

  @override
  set expandRefPanel(bool value) {
    _$expandRefPanelAtom.context.conditionallyRunInAction(() {
      super.expandRefPanel = value;
      _$expandRefPanelAtom.reportChanged();
    }, _$expandRefPanelAtom, name: '${_$expandRefPanelAtom.name}_set');
  }

  final _$expandFilePanelAtom = Atom(name: '_JobOrderStore.expandFilePanel');

  @override
  bool get expandFilePanel {
    _$expandFilePanelAtom.context.enforceReadPolicy(_$expandFilePanelAtom);
    _$expandFilePanelAtom.reportObserved();
    return super.expandFilePanel;
  }

  @override
  set expandFilePanel(bool value) {
    _$expandFilePanelAtom.context.conditionallyRunInAction(() {
      super.expandFilePanel = value;
      _$expandFilePanelAtom.reportChanged();
    }, _$expandFilePanelAtom, name: '${_$expandFilePanelAtom.name}_set');
  }

  final _$expandDetailsPanelAtom =
      Atom(name: '_JobOrderStore.expandDetailsPanel');

  @override
  bool get expandDetailsPanel {
    _$expandDetailsPanelAtom.context
        .enforceReadPolicy(_$expandDetailsPanelAtom);
    _$expandDetailsPanelAtom.reportObserved();
    return super.expandDetailsPanel;
  }

  @override
  set expandDetailsPanel(bool value) {
    _$expandDetailsPanelAtom.context.conditionallyRunInAction(() {
      super.expandDetailsPanel = value;
      _$expandDetailsPanelAtom.reportChanged();
    }, _$expandDetailsPanelAtom, name: '${_$expandDetailsPanelAtom.name}_set');
  }

  final _$expandRemarksPanelAtom =
      Atom(name: '_JobOrderStore.expandRemarksPanel');

  @override
  bool get expandRemarksPanel {
    _$expandRemarksPanelAtom.context
        .enforceReadPolicy(_$expandRemarksPanelAtom);
    _$expandRemarksPanelAtom.reportObserved();
    return super.expandRemarksPanel;
  }

  @override
  set expandRemarksPanel(bool value) {
    _$expandRemarksPanelAtom.context.conditionallyRunInAction(() {
      super.expandRemarksPanel = value;
      _$expandRemarksPanelAtom.reportChanged();
    }, _$expandRemarksPanelAtom, name: '${_$expandRemarksPanelAtom.name}_set');
  }

  final _$jobOrderSaveActionAtom =
      Atom(name: '_JobOrderStore.jobOrderSaveAction');

  @override
  String get jobOrderSaveAction {
    _$jobOrderSaveActionAtom.context
        .enforceReadPolicy(_$jobOrderSaveActionAtom);
    _$jobOrderSaveActionAtom.reportObserved();
    return super.jobOrderSaveAction;
  }

  @override
  set jobOrderSaveAction(String value) {
    _$jobOrderSaveActionAtom.context.conditionallyRunInAction(() {
      super.jobOrderSaveAction = value;
      _$jobOrderSaveActionAtom.reportChanged();
    }, _$jobOrderSaveActionAtom, name: '${_$jobOrderSaveActionAtom.name}_set');
  }

  final _$jobOrderDetailsObservableAtom =
      Atom(name: '_JobOrderStore.jobOrderDetailsObservable');

  @override
  ObservableList<JobOrderDetailObj> get jobOrderDetailsObservable {
    _$jobOrderDetailsObservableAtom.context
        .enforceReadPolicy(_$jobOrderDetailsObservableAtom);
    _$jobOrderDetailsObservableAtom.reportObserved();
    return super.jobOrderDetailsObservable;
  }

  @override
  set jobOrderDetailsObservable(ObservableList<JobOrderDetailObj> value) {
    _$jobOrderDetailsObservableAtom.context.conditionallyRunInAction(() {
      super.jobOrderDetailsObservable = value;
      _$jobOrderDetailsObservableAtom.reportChanged();
    }, _$jobOrderDetailsObservableAtom,
        name: '${_$jobOrderDetailsObservableAtom.name}_set');
  }

  final _$selectedJobOrderDetailsAtom =
      Atom(name: '_JobOrderStore.selectedJobOrderDetails');

  @override
  ObservableList<JobOrderDetailObj> get selectedJobOrderDetails {
    _$selectedJobOrderDetailsAtom.context
        .enforceReadPolicy(_$selectedJobOrderDetailsAtom);
    _$selectedJobOrderDetailsAtom.reportObserved();
    return super.selectedJobOrderDetails;
  }

  @override
  set selectedJobOrderDetails(ObservableList<JobOrderDetailObj> value) {
    _$selectedJobOrderDetailsAtom.context.conditionallyRunInAction(() {
      super.selectedJobOrderDetails = value;
      _$selectedJobOrderDetailsAtom.reportChanged();
    }, _$selectedJobOrderDetailsAtom,
        name: '${_$selectedJobOrderDetailsAtom.name}_set');
  }

  final _$jobOrderFilesObservableAtom =
      Atom(name: '_JobOrderStore.jobOrderFilesObservable');

  @override
  ObservableList<JobOrderFileObj> get jobOrderFilesObservable {
    _$jobOrderFilesObservableAtom.context
        .enforceReadPolicy(_$jobOrderFilesObservableAtom);
    _$jobOrderFilesObservableAtom.reportObserved();
    return super.jobOrderFilesObservable;
  }

  @override
  set jobOrderFilesObservable(ObservableList<JobOrderFileObj> value) {
    _$jobOrderFilesObservableAtom.context.conditionallyRunInAction(() {
      super.jobOrderFilesObservable = value;
      _$jobOrderFilesObservableAtom.reportChanged();
    }, _$jobOrderFilesObservableAtom,
        name: '${_$jobOrderFilesObservableAtom.name}_set');
  }

  final _$fetchJobOrdersAsyncAction = AsyncAction('fetchJobOrders');

  @override
  Future<void> fetchJobOrders() {
    return _$fetchJobOrdersAsyncAction.run(() => super.fetchJobOrders());
  }

  final _$fetchJobOrderByIDAsyncAction = AsyncAction('fetchJobOrderByID');

  @override
  Future<void> fetchJobOrderByID(int jobOrderID) {
    return _$fetchJobOrderByIDAsyncAction
        .run(() => super.fetchJobOrderByID(jobOrderID));
  }

  final _$fetchJobOrderByLastModifiedDateTimeAsyncAction =
      AsyncAction('fetchJobOrderByLastModifiedDateTime');

  @override
  Future<void> fetchJobOrderByLastModifiedDateTime(String modifiedDateTime) {
    return _$fetchJobOrderByLastModifiedDateTimeAsyncAction
        .run(() => super.fetchJobOrderByLastModifiedDateTime(modifiedDateTime));
  }

  final _$insertJobOrderAsyncAction = AsyncAction('insertJobOrder');

  @override
  Future<void> insertJobOrder() {
    return _$insertJobOrderAsyncAction.run(() => super.insertJobOrder());
  }

  final _$deleteJobOrderAsyncAction = AsyncAction('deleteJobOrder');

  @override
  Future<void> deleteJobOrder(JobOrderObj jobOrder) {
    return _$deleteJobOrderAsyncAction
        .run(() => super.deleteJobOrder(jobOrder));
  }

  final _$updateJobOrderAsyncAction = AsyncAction('updateJobOrder');

  @override
  Future<void> updateJobOrder(JobOrderObj jobOrder) {
    return _$updateJobOrderAsyncAction
        .run(() => super.updateJobOrder(jobOrder));
  }

  final _$fetchJobOrderDetailsAsyncAction = AsyncAction('fetchJobOrderDetails');

  @override
  Future<void> fetchJobOrderDetails() {
    return _$fetchJobOrderDetailsAsyncAction
        .run(() => super.fetchJobOrderDetails());
  }

  final _$deleteJobOrderDetailAsyncAction = AsyncAction('deleteJobOrderDetail');

  @override
  Future<void> deleteJobOrderDetail(JobOrderDetailObj jobOrderDetail) {
    return _$deleteJobOrderDetailAsyncAction
        .run(() => super.deleteJobOrderDetail(jobOrderDetail));
  }

  final _$fetchJobOrderFilesAsyncAction = AsyncAction('fetchJobOrderFiles');

  @override
  Future<void> fetchJobOrderFiles() {
    return _$fetchJobOrderFilesAsyncAction
        .run(() => super.fetchJobOrderFiles());
  }

  final _$_JobOrderStoreActionController =
      ActionController(name: '_JobOrderStore');

  @override
  void validateCustomerCode(String str) {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.validateCustomerCode(str);
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateJobOrderNbr(String str) {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.validateJobOrderNbr(str);
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateJobOrderDate(String str) {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.validateJobOrderDate(str);
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateRequesterID(String str) {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.validateRequesterID(str);
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validateAllForJobOrder() {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.validateAllForJobOrder();
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetJobOrder() {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.resetJobOrder();
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setJobOrderLastModifiedDateTime(String str) {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.setJobOrderLastModifiedDateTime(str);
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetSelectedJobOrderDetails() {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.resetSelectedJobOrderDetails();
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetJobOrderDetailObservable() {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.resetJobOrderDetailObservable();
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetJobOrderFilesObservable() {
    final _$actionInfo = _$_JobOrderStoreActionController.startAction();
    try {
      return super.resetJobOrderFilesObservable();
    } finally {
      _$_JobOrderStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'jobOrderError: ${jobOrderError.toString()},jobOrders: ${jobOrders.toString()},jobOrdersObservable: ${jobOrdersObservable.toString()},jobOrder: ${jobOrder.toString()},expandRefPanel: ${expandRefPanel.toString()},expandFilePanel: ${expandFilePanel.toString()},expandDetailsPanel: ${expandDetailsPanel.toString()},expandRemarksPanel: ${expandRemarksPanel.toString()},jobOrderSaveAction: ${jobOrderSaveAction.toString()},jobOrderDetailsObservable: ${jobOrderDetailsObservable.toString()},selectedJobOrderDetails: ${selectedJobOrderDetails.toString()},jobOrderFilesObservable: ${jobOrderFilesObservable.toString()},hasJobOrderValidationError: ${hasJobOrderValidationError.toString()}';
    return '{$string}';
  }
}

mixin _$JobOrderErrorState on _JobOrderErrorState, Store {
  final _$custCodeAtom = Atom(name: '_JobOrderErrorState.custCode');

  @override
  String get custCode {
    _$custCodeAtom.context.enforceReadPolicy(_$custCodeAtom);
    _$custCodeAtom.reportObserved();
    return super.custCode;
  }

  @override
  set custCode(String value) {
    _$custCodeAtom.context.conditionallyRunInAction(() {
      super.custCode = value;
      _$custCodeAtom.reportChanged();
    }, _$custCodeAtom, name: '${_$custCodeAtom.name}_set');
  }

  final _$jobOrderNbrAtom = Atom(name: '_JobOrderErrorState.jobOrderNbr');

  @override
  String get jobOrderNbr {
    _$jobOrderNbrAtom.context.enforceReadPolicy(_$jobOrderNbrAtom);
    _$jobOrderNbrAtom.reportObserved();
    return super.jobOrderNbr;
  }

  @override
  set jobOrderNbr(String value) {
    _$jobOrderNbrAtom.context.conditionallyRunInAction(() {
      super.jobOrderNbr = value;
      _$jobOrderNbrAtom.reportChanged();
    }, _$jobOrderNbrAtom, name: '${_$jobOrderNbrAtom.name}_set');
  }

  final _$jobOrderDateAtom = Atom(name: '_JobOrderErrorState.jobOrderDate');

  @override
  String get jobOrderDate {
    _$jobOrderDateAtom.context.enforceReadPolicy(_$jobOrderDateAtom);
    _$jobOrderDateAtom.reportObserved();
    return super.jobOrderDate;
  }

  @override
  set jobOrderDate(String value) {
    _$jobOrderDateAtom.context.conditionallyRunInAction(() {
      super.jobOrderDate = value;
      _$jobOrderDateAtom.reportChanged();
    }, _$jobOrderDateAtom, name: '${_$jobOrderDateAtom.name}_set');
  }

  final _$requesterIDAtom = Atom(name: '_JobOrderErrorState.requesterID');

  @override
  String get requesterID {
    _$requesterIDAtom.context.enforceReadPolicy(_$requesterIDAtom);
    _$requesterIDAtom.reportObserved();
    return super.requesterID;
  }

  @override
  set requesterID(String value) {
    _$requesterIDAtom.context.conditionallyRunInAction(() {
      super.requesterID = value;
      _$requesterIDAtom.reportChanged();
    }, _$requesterIDAtom, name: '${_$requesterIDAtom.name}_set');
  }

  @override
  String toString() {
    final string =
        'custCode: ${custCode.toString()},jobOrderNbr: ${jobOrderNbr.toString()},jobOrderDate: ${jobOrderDate.toString()},requesterID: ${requesterID.toString()}';
    return '{$string}';
  }
}
