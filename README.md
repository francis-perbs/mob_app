# mob_app

MobAPP - Mobile Attendance Profile Program

A flutter project developed mainly for Android (may be ported to iOS) that can be used to file employee attendance and expense claim via smartphone application.

## Getting Started

This project is a starting point for my flutter programming journey.
The program is a mobile app that employees can use for filing:

* Job Orders
* Vacation Leaves
* Overtimes
* Expense Claims

The mobile app will have offline features to save the created documents locally.
Users can then submit the documents online to a designated API backend.

### Screenshots
![Home Screen](/images/screenshots/home_300.png)
![Job Order List](/images/screenshots/job_order_list_300.png)
![New Job Order Collapsed](/images/screenshots/new_job_order_overview_300.png)
![New Job Order References](/images/screenshots/new_job_order_ref_300.png)
![New Job Order Files](/images/screenshots/new_job_order_file_300.png)
![New Job Order Details](/images/screenshots/new_job_order_detail_300.png)
![New Job Order Remarks](/images/screenshots/new_job_order_remark_300.png)
![View Job Order Detail Delete](/images/screenshots/job_order_detail_delete_300.png)
![View Job Order File Delete](/images/screenshots/job_order_file_deleted_300.png)
![View Job Order Updated](/images/screenshots/job_order_updated_300.png)

### Installing

1. Clone the project using:
> git clone https://gitlab.com/francis-perbs/mob_app.git

2. cd into the mob_app folder:
> cd mob_app

3. Download the necessary packages:
> flutter pub get

4. Run the program:
> flutter run

### Authors

* **Francis Pervera**

### License

This project is licensed under the MIT License - see the LICENSE file for more details.